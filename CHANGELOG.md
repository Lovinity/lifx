# 4.1.0 beta - UNRELEASED

## Added

- HueManager.addReturnColorCycle which should be called when we want to return a bulb to its returnColor state (if set) and then reset returnColor
- Mutex locking for Hue calls to help reduce the risk of API errors and limits

## Changed

- setAsReturnColor in a cycle is now a number {0 | 1 | 2}; 0 = no, 1 = yes if another cycle has not already set this, 2 = yes, and override existing setting
- Adjusted code so that hopefully bulbs actually transition back to original color after lightning / precipitation ends
- Precipitation detected now takes priority over lightning in the near-term forecast for bulbs that are both precipitation and lightning (but lightning detected still takes priority over everything, and lightning in the forecast takes priority over precipitation in the forecast)
- 5-minute weather alert cycle now also filters bulbs by alert severity; e.g. if a minor alert is in effect and a 5-minute bulb is not set to alert for minor alerts, it will also not cycle the minor alert in the 5-minute routine.
- Updated list of National Weather Service alerts and colors
- Added more Toast notifications during the local connection Bridge account creation process so it does not look like it's frozen.

## Fixed

- New weather alert flashing was setting the "off" property to true instead of undefined, triggering an infinite loop of backup cycle flashing because you cannot change the hue on an off bulb.
- Added a prevention within changing Hue bulb state where if the bulb is off, and we are not powering it on, we will skip it to prevent Hue throwing an error and triggering a backup cycle loop.
- A broken remote API configuration (invalid access token) was causing the app not to try the local connection even if configured. A fix makes the app disable trying the remote API if the access token is invalid (so we can start using the local API instead).
- If we have a cached previous state for a bulb available (e.g. a cycle is running), use that instead of trying to get the current state and potentially overriding the color and state we are supposed to return to.

## Removed

- Red flashing when a bulb is deemed not connected

# 4 beta

- Support for remote or local connections to Hue

# 3.1.0-alpha - 2023-04-14

## Added

- HueBulb.returnColor and HueCycle.cycle.setAsReturnColor for remembering what color the bulbs should be reset to after an overriding state, such as lightning, ends.

## Changed

- Precipitation / lightning detected pulse colors: Lightning only = yellow, lightning + precipitation = green, precipitation only = cyan.

# 3.0.0-alpha - 2023-04

## Changed

- [BREAKING] Removed OpenWeatherMap (moved to its own branch); replaced with Tempest / Weatherflow API for Tempest weather stations
- [BREAKING] Added / modified precipitation, lightning, and brightness control based on Tempest station data

## Updated

- NPM packages to their latest versions

# 2.0.3 - 2022-07-21

## Changed

- App now uses UDP port 56701 instead of port 0. Added info in README about allowing this port through firewalls.

## Updated

- NPM packages to their latest versions

# 2.0.2 - 2022-06-08

## Changed

- The National Weather Service is now categorizing Tornado Watches as Extreme alerts, causing bulbs to flash every minute. Our intent was for bulbs to only do that for extreme, immediate-action alerts. Tweaked criteria so that alert must be Extreme AND urgent / fairly certain for bulbs to flash every minute.

# 2.0.1 - 2022-06-02

## Fixed

- shell is not defined in main process when clicking a link.

# 2.0.0 - 2022-06-02

## Removed

- [BREAKING] node-cron replaced by HueScheduleManager and later.js

## Added

- Weather: Configure bulbs to gently flash for current or incoming precipitation.
- HueCycle: Ability to use objects in hue, saturation, brightness, and kelvin for color and from_color where key = arithmetic symbol (+-\*/) and value is amount, to use the current bulb's value with math applied to it.
- HueBulb.returnState function; can be set to a function to execute something ultra-low priority when a bulb finishes all its cycles.
- HueScheduleManager, HueSchedule, and later.js for scheduling tasks such as bulb color/power changing and flashing.

## Changed

- Increased frequency of bulb cycle checks
- Bulb color changing now uses HueBulb.returnState instead of queuing a cycle.
- Use crypto when possible for generating UUIDs.
- Use the maximum cloud cover between current, last hour, and next hour from OpenWeatherMap for determining brightness.

## Fixed

- HueCycle.highPriority did not exist when it should.
- If a cached bulb is not discovered on the first bulb state update, the lifx lan library throws an error. Added an error exception to just report the bulb as offline and queue cycles in the backup.

# 1.0.0 - 2022-05-12

INITIAL RELEASE
