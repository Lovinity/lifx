# HUE TEMPEST WEATHER CONTROLLER

This is a simple Electron.js application for controlling your Philips Hue smart bulbs on a local network. While it features standard controls like toggling on/off and changing color, it features advanced tools:

- Control bulb brightness based on the sun position
- Control white light warmth (kelvin) based on the time
- Flash bulbs for weather alerts (National Weather Service / US Only)
- Create complex Cron-style schedules to turn bulbs on/off, change their color, or flash them. Supports up to second precision. You can also use equations (e.g. +10 to increase a hue/saturation/brightness/kelvin by 10, or *2 to multiply it by 2).

If you have a Tempest weather station:
- Adjust bulb brightness according to detected solar brightness (the brighter it is outside, the dimmer your bulbs will get)
 - Great for dimming your bulbs on a sunny day and having them get bright again when, say, a storm comes in
- Set bulbs to indicate forecast or detected lightning or precipitation (and return to their original color when no-longer in the forecast)

The software uses a cycle queue to help ensure flash cycles or schedules execute when possible even when a bulb goes offline temporarily. For example, if a weather alert gets issued and a bulb experiences an error when trying to flash it, the flash cycle will be added to a "backup queue" which will fire when the bulb is responding again.

More features are to come!

For more information and the full list of features, see https://pdstig.me/catalogues/entry/projects-home/hue-weather-controller.htm.