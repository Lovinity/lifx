// Electroncalculations
const { app, BrowserWindow, ipcMain, shell, session } = require("electron");

// app config
const appConfig = require("./config.json");

// Electron store
const Store = require("electron-store");

// Node.js
const path = require("path");
const querystring = require("node:querystring");

// suncalc for performing sun position and strength calculations
const SunCalc = require("suncalc");

// Luxon datetime tool for date/time management
const { DateTime } = require("luxon");

// general HTTP requests
const fetch = require("node-fetch-commonjs");

// Tempest
let cachedObservations = [];
let cachedWeather = {};
let cachedForecast = {};
let lastWeatherUpdate = 0;

// NWS Wrapper
const { Client } = require("weathered");
let nws = new Client();
let cachedWeatherAlerts = [];

// Philips Hue
const nodeHue = require("node-hue-api");
const nodeHueRemote = nodeHue.api.createRemote(
  appConfig.clientId,
  appConfig.clientSecret
);
let nodeHueRemoteApi;
let nodeHueRemoteRefresh = 0;

const { machineIdSync } = require("node-machine-id");
const randomstring = require("randomstring");

// Mutex function locking / queue
const { Mutex, withTimeout } = require("async-mutex");
const mutex = withTimeout(new Mutex(), 60000);

const appName = "hue-weather-controller";
const deviceName = machineIdSync().substring(0, 16);

// Variables

// Initialize settings store
const store = new Store({
  name: "controller-hue-tempest",
  clearInvalidConfig: true,
  defaults: {
    bulbs: {},
    weatherAlerts: [], // Active weather alerts
    oAuth: {
      hue: {},
    },
    settings: {
      hue: {
        ipAddress: "",
        username: "",
        clientKey: "",
        remoteCode: "",
        remoteClientId: "",
        remoteClientSecret: "",
        remoteUsername: "",
      },
      schedules: [], // Scheduled operations
      location: {
        lat: 0,
        lon: 0,
      },
      sun: {
        bulbsSun: [], // Array of bulbs that should adjust brightness according to sun position
        bulbsKelvin: [], // Array of bulbs that should adjust kelvin according to the current time
        sun: {
          minBrightness: 0, // Minimum brightness for sun position
          maxBrightness: 100, // Maximum brightness for sun position
        },
        kelvin: {
          min: 2000, // Minimum Kelvin
          max: 6500, // Maximum Kelvin
          minAtHour: 23, // Reach minimum at this hour
          maxAtHour: 7, // Switch from minimum to maximum at this hour
        },
      },
      weather: {
        tempest: {
          // Tempest weatherflow station and token
          stationId: "",
          token: "",
        },
        bulbsSunBrightness: [], // Array of bulbs that should adjust brightness according to current sun brightness
        bulbsPrecipitation: [], // Array of bulbs that should gently pulse when precipitation is falling
        bulbsLightning: [], // Array of bulbs that will turn on / pastel yellow when lightning is detected to drown it out
        sunBrightness: {
          minBrightness: 0,
          maxBrightness: 100,
          minLux: 0,
          maxLux: 25000,
        },
        lightning: {
          maxTime: 60 * 30, // Lightning must have been detected within this many seconds for it to trigger lightning alerts
        },
      },
      NWS: {
        bulbs: {
          // Light bulbs by severity of weather alert
          Extreme: [],
          Severe: [],
          Moderate: [],
          Minor: [],
          sequence: [], // Bulbs set to flash through active alerts every 5 minutes and set its color according to most severe alert
        },
        alerts: {
          // Which alerts should flash the bulbs?
          "Tsunami Warning": true,
          "Tornado Warning": true,
          "Extreme Wind Warning": true,
          "Severe Thunderstorm Warning": true,
          "Flash Flood Warning": true,
          "Flash Flood Statement": true,
          "Severe Weather Statement": true,
          "Shelter In Place Warning": true,
          "Evacuation Immediate": true,
          "Civil Danger Warning": true,
          "Nuclear Power Plant Warning": true,
          "Radiological Hazard Warning": true,
          "Hazardous Materials Warning": true,
          "Fire Warning": true,
          "Civil Emergency Message": true,
          "Law Enforcement Warning": true,
          "Storm Surge Warning": true,
          "Hurricane Force Wind Warning": true,
          "Hurricane Warning": true,
          "Typhoon Warning": true,
          "Special Marine Warning": true,
          "Blizzard Warning": true,
          "Snow Squall Warning": true,
          "Ice Storm Warning": true,
          "Heavy Freezing Spray Warning": true,
          "Winter Storm Warning": true,
          "Lake Effect Snow Warning": true,
          "Dust Storm Warning": true,
          "Blowing Dust Warning": true,
          "High Wind Warning": true,
          "Tropical Storm Warning": true,
          "Storm Warning": true,
          "Tsunami Advisory": true,
          "Tsunami Watch": true,
          "Avalanche Warning": true,
          "Earthquake Warning": true,
          "Volcano Warning": true,
          "Ashfall Warning": true,
          "Flood Warning": true,
          "Coastal Flood Warning": true,
          "Lakeshore Flood Warning": true,
          "Ashfall Advisory": true,
          "High Surf Warning": true,
          "Excessive Heat Warning": true,
          "Tornado Watch": true,
          "Severe Thunderstorm Watch": true,
          "Flash Flood Watch": true,
          "Gale Warning": true,
          "Flood Statement": true,
          "Extreme Cold Warning": true,
          "Freeze Warning": true,
          "Red Flag Warning": true,
          "Storm Surge Watch": true,
          "Hurricane Watch": true,
          "Hurricane Force Wind Watch": true,
          "Typhoon Watch": true,
          "Tropical Storm Watch": true,
          "Storm Watch": true,
          "Tropical Cyclone Local Statement": true,
          "Winter Weather Advisory": true,
          "Avalanche Advisory": true,
          "Cold Weather Advisory": true,
          "Heat Advisory": true,
          "Flood Advisory": true,
          "Coastal Flood Advisory": true,
          "Lakeshore Flood Advisory": true,
          "High Surf Advisory": true,
          "Dense Fog Advisory": true,
          "Dense Smoke Advisory": true,
          "Small Craft Advisory": true,
          "Brisk Wind Advisory": true,
          "Hazardous Seas Warning": true,
          "Dust Advisory": true,
          "Blowing Dust Advisory": true,
          "Lake Wind Advisory": true,
          "Wind Advisory": true,
          "Frost Advisory": true,
          "Freezing Fog Advisory": true,
          "Freezing Spray Advisory": true,
          "Low Water Advisory": true,
          "Local Area Emergency": true,
          "Winter Storm Watch": true,
          "Rip Current Statement": true,
          "Beach Hazards Statement": true,
          "Gale Watch": true,
          "Avalanche Watch": true,
          "Hazardous Seas Watch": true,
          "Heavy Freezing Spray Watch": true,
          "Flood Watch": true,
          "Coastal Flood Watch": true,
          "Lakeshore Flood Watch": true,
          "High Wind Watch": true,
          "Excessive Heat Watch": true,
          "Extreme Cold Watch": true,
          "Freeze Watch": true,
          "Fire Weather Watch": true,
          "Extreme Fire Danger": true,
          "911 Telephone Outage": true,
          "Coastal Flood Statement": true,
          "Lakeshore Flood Statement": true,
          "Special Weather Statement": true,
          "Marine Weather Statement": true,
          "Air Quality Alert": true,
          "Air Stagnation Advisory": true,
          "Hazardous Weather Outlook": true,
          "Hydrologic Outlook": true,
          "Short Term Forecast": true,
          "Administrative Message": true,
          Test: true,
          "Child Abduction Emergency": true,
          "Blue Alert": true,
        },
      },
    },
  },

  // TODO: Keep this updated
  migrations: {
    ">=4.0.0": (store) => {
      store.clear(); // Philips Hue instead of LIFX.
    },
    ">=3.0.0": (store) => {
      store.clear(); // Whole new Tempest station premise instead of Openweathermap.
    },
    ">=2.0.0": (store) => {
      console.log(`MIGRATION ACTIVATED, v2.0.0.`);
      if (!store.has(`settings.schedules`)) store.set("settings.schedules", []);
      if (!store.has(`settings.weather.bulbsPrecipitation`))
        store.set("settings.weather.bulbsPrecipitation", []);
    },
  },
});

// bulbs cache
let bulbs = {};

// Populate bulb cache with settings
let temp = store.get(`bulbs`);
for (let setting in temp) {
  if (Object.prototype.hasOwnProperty.call(temp, setting)) {
    if (typeof bulbs[setting] === "undefined") {
      bulbs[setting] = {
        id: temp[setting].id,
        uniqueid: temp[setting].uniqueid,
        name: temp[setting].name,
        online: -1,
      };
    }
  }
}

let mainWindow;
let oauthWindow;
let csrf = "";
function createWindow() {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1280,
    height: 720,
    show: false, // Do not show until we are ready to show via ready-to-show event
    title: `Hue Tempest Weather Controller`,
    autoHideMenuBar: true, // Do not show manu bar unless alt is pressed
    webPreferences: {
      contextIsolation: true,
      enableRemoteModule: false, // electron's remote module is insecure
      backgroundThrottling: false, // Timers need to keep running in renderer
      preload: path.join(__dirname, "preload.js"),
      sandbox: true,
      devTools: true,
    },
  });

  mainWindow.once("ready-to-show", () => {
    mainWindow.show();
    // mainWindow.webContents.openDevTools();
  });

  mainWindow.on("closed", () => {
    app.quit();
  });

  // and load the index.html of the app.
  mainWindow.loadFile("index.html");

  // Renderer should not allow navigation to any external websites
  mainWindow.webContents.on("will-navigate", (event) => {
    event.preventDefault(); // AUXCLICK_JS_CHECK
  });
}

function createOauthWindow() {
  // Generate CSRF
  csrf = randomstring.generate();

  // Create the browser window.
  oauthWindow = new BrowserWindow({
    show: false, // Do not show until we are ready to show via ready-to-show event
    title: `Philips Hue OAuth`,
    autoHideMenuBar: true, // Do not show manu bar unless alt is pressed
    webPreferences: {
      nodeIntegration: false,
      enableRemoteModule: false,
    },
  });

  console.log("created");
  oauthWindow.once("ready-to-show", () => {
    console.log("ready to show");
    oauthWindow.show();
  });

  oauthWindow.loadURL(nodeHueRemote.getAuthCodeUrl(deviceName, appName, csrf));

  oauthWindow.on("closed", () => {
    oauthWindow = undefined;
  });

  oauthWindow.webContents.on("will-navigate", (event) => {
    if (
      !event.url.startsWith("https://api.meethue.com/") &&
      !event.url.startsWith("https://account.meethue.com/") &&
      !event.url.startsWith("https://auth.meethue.com/")
    ) {
      console.log(event.url & " BLOCKED");
      event.preventDefault();
    }

    if (event.url.startsWith("http://localhost")) {
      let query = querystring.parse(event.url);
      if (query.state !== csrf) {
        oauthWindow.close();
        mainWindow.webContents.send("log", [
          "error",
          `Main -> oAuth; failed to authorize app with Hue; the CSRF token did not match.`,
          true,
        ]);
        return;
      }

      if (!query.code) {
        oauthWindow.close();
        mainWindow.webContents.send("log", [
          "error",
          `Main -> oAuth; failed to authorize app with Hue; a code was not returned.`,
          true,
        ]);
        return;
      } else {
        nodeHueRemote
          .connectWithCode(query.code)
          .then((api) => {
            nodeHueRemoteApi = api;
            oauthWindow.close();

            let config = nodeHueRemoteApi.remote.getRemoteAccessCredentials();
            store.set("oAuth.hue", {
              accessToken: config.tokens.access,
              refreshToken: config.tokens.refresh,
              username: config.username,
            });

            mainWindow.webContents.send("log", [
              "success",
              `Main -> oAuth; application successfully authorized with Hue.`,
              true,
            ]);
          })
          .catch((error) => {
            oauthWindow.close();

            mainWindow.webContents.send("log", [
              "error",
              `Main -> oAuth; failed to authorize app with Hue; ${error.message}`,
              true,
            ]);
          });
      }
    }
  });
}

// Enforce sandboxing for security (every process is in its own isolated environment)
app.enableSandbox();

// Prevent multiple instances of the app from running
if (!app.requestSingleInstanceLock()) {
  app.quit();
}

// If a second instance is spawned, show the current instance in the event it is minimized or hidden
app.on("second-instance", () => {
  if (mainWindow) {
    if (mainWindow.isMinimized()) {
      mainWindow.restore();
    }

    mainWindow.show();
  }
});

// Prevent opening new windows in the app browsers; use the default browser instead
app.on("web-contents-created", (event, contents) => {
  contents.setWindowOpenHandler((details) => {
    // Allow loading of files
    if (details.url.startsWith("file://")) {
      return { action: "allow" };
    }

    // Do not navigate to anything other than https protocol sites
    if (details.url.startsWith("https://")) {
      shell.openExternal(details.url);
    }

    return { action: "deny" };
  });
});

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(() => {
  createWindow();

  app.on("activate", function () {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow();
  });
});

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on("window-all-closed", function () {
  if (process.platform !== "darwin") app.quit();
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.

// IPC events

let activeCycles = [];
let prevBulbStates = {};
ipcMain.on("main", (event, args) => {
  let cycle;
  let settings;
  let locationSettings;
  let stage1;
  let stage2;
  let changeState;
  let sendToBackup;

  switch (args[0]) {
    case "renderer-ready":
      setTimeout(() => {
        // Renderer reports ready; send initial bulb data and initialize Hue / Tempest
        try {
          let _bulbs = [];
          settings = store.get(`bulbs`);
          for (let setting in settings) {
            if (Object.prototype.hasOwnProperty.call(settings, setting)) {
              _bulbs.push({
                id: settings[setting].id,
                uniqueid: settings[setting].uniqueid,
                name: settings[setting].name,
                online: -1,
              });
            }
          }
          mainWindow.webContents.send("bulbs", _bulbs);
          mainWindow.webContents.send("settings", store.get(`settings`));
        } catch (e) {
          console.error(e);
        }
        discoverHueBulbs(true);
      }, 3000);
      break;

    case "one-minute-tasks":
      oneMinuteTasks();
      break;

    case "five-minute-tasks":
      fiveMinuteTasks();
      break;

    case "update-bulbs":
      discoverHueBulbs();
      break;

    case "create-user":
      createHueUser();
      break;

    case "discover":
      discoverBridge();
      break;

    case "oauth-hue":
      if (typeof oauthWindow === "undefined") createOauthWindow();
      break;

    // (sync) Get sun data. Arg[0] = ISO string of when to get sun data
    case "get-sun-data":
      locationSettings = store.get(`settings.location`);
      sunData = SunCalc.getTimes(
        new Date(args[1][0]),
        locationSettings.lat,
        locationSettings.lon
      );
      event.returnValue = sunData;
      break;

    // Settings tasks

    // Set new settings and transmit the current / updated settings back
    case "settings":
      store.set(`settings.${args[1][0]}`, args[1][1]);

      if (args[1][0] !== "schedules") {
        // Do not transmit schedule changes; we already know these and schedules are saved every time one runs.
        mainWindow.webContents.send("settings", store.get(`settings`));
      }

      // If hue IP Address was blanked, try to find it.
      if (
        args[1][0] === "hue" &&
        store.get("settings.hue.ipAddress", "") === ""
      ) {
        discoverBridge();
      }
      break;

    // bulb tasks
    case "state":
      // Ignore if we have no bulbs.
      if (!args[1][0].bulbs || args[1][0].bulbs.length === 0) break;

      cycle = args[1][0].cycle;

      if (typeof cycle.state === "undefined") cycle.state = {};

      let delay = 1000;
      if (
        typeof cycle.state.alert !== "undefined" &&
        cycle.state.alert !== "none" &&
        (!args[1][0].stage || args[1][0].stage === 1)
      ) {
        delay = cycle.state.alert === "lselect" ? 17000 : 3000;
      }

      mainWindow.webContents.send("log", [
        "console",
        `Main: cycle id ${args[1][0].id}`,
      ]);

      activeCycles.push(args[1][0]);

      // Define backup function
      sendToBackup = (device, stage, prevState) => {
        delete prevBulbStates[device];
        mainWindow.webContents.send("backupCycle", [
          Object.assign({}, args[1][0], {
            bulbs: [device],
            stage: stage,
            prevState: prevState,
          }),
        ]);
        bulbs[device].online = bulbs[device].online > -1 ? 0 : -1;
        mainWindow.webContents.send("bulb", {
          uniqueid: device,
          online: bulbs[device].online > -1 ? 0 : -1,
        });
      };

      // Define function for running the change state on repeat
      changeState = (device, color, cycleI, prevState) => {
        if (
          activeCycles.some((cyc) => {
            if (cyc.id === args[1][0].id) return false;
            if (cyc.bulbs.indexOf(device) === -1) return false;
            if (cyc.cycle.highPriority && !cycleI.highPriority) return true;
            return false;
          })
        ) {
          activeCycles = activeCycles.filter((cyc) => cyc.id !== args[1][0].id);
          mainWindow.webContents.send("log", [
            "console",
            `Main: cycle id ${args[1][0].id} stopped; another high-priority cycle is now running for this bulb.`,
          ]);
          return;
        }

        changeHueBulbState(device, color)
          .then((result) => {
            if (result) {
              // Decrease repeat count for each time we ran this
              if (typeof cycleI.repeat !== "undefined") {
                cycleI.repeat--;
                mainWindow.webContents.send("log", [
                  "console",
                  `Main: cycle id ${args[1][0].id} executed; ${cycleI.repeat} executions left.`,
                ]);
              }
              setTimeout(() => {
                // Go to stage 2 if we are not to run this again, else run it again (after the delay)
                if (
                  typeof cycleI.repeat === "undefined" ||
                  cycleI.repeat <= 0
                ) {
                  if (cycle.setAsReturnColor !== 0) {
                    mainWindow.webContents.send("returnColor", {
                      bulb: device,
                      state: color,
                      overrideExisting: cycle.setAsReturnColor === 2,
                    });
                  }
                  stage2(device, prevState);
                } else {
                  changeState(device, color, cycleI, prevState);
                }
              }, delay);
            } else {
              mainWindow.webContents.send("log", [
                "warn",
                `Main -> cycle ${args[1][0].id}: Failed on bulb ${device}. Added to backup cycle.`,
              ]);
              sendToBackup(device, 1, prevState);
            }
          })
          .catch((err) => {
            mainWindow.webContents.send("log", [
              "warn",
              `Main -> cycle ${args[1][0].id}: Failed on bulb ${device}. Added to backup cycle.`,
            ]);
            sendToBackup(device, 1, prevState);
          });
      };

      // Define stage 1 function: change the state
      stage1 = (device, prevState, _cycle) => {
        let cycleI = Object.assign({}, _cycle);

        mainWindow.webContents.send("log", [
          "console",
          `Main -> cycle ${args[1][0].id}: changing state.`,
        ]);

        // If power is specified, overwrite state.on
        if (typeof cycleI.power !== "undefined") {
          switch (cycleI.power) {
            case 0:
              cycleI.state.on = false;
              break;
            case 1:
              cycleI.state.on = true;
              break;
            case 2:
              cycleI.state.on = !prevState.on;
              break;
          }
        }

        // No execution if flashing on a bulb that is / will be off
        if (
          (cycleI.state.alert === "select" ||
            cycleI.state.alert === "lselect") &&
          (typeof cycleI.state === "undefined" ||
            typeof cycleI.state.on === "undefined" ||
            cycleI.state.on === false) &&
          !prevState.on
        ) {
          mainWindow.webContents.send("log", [
            "warn",
            `Main -> cycle ${args[1][0].id}: skipped bulb ${device}; trying to flash on a bulb that is (or will be) off.`,
          ]);
          return;
        }

        let color = determineHueColor(cycleI.state, prevState);

        changeState(device, color, cycleI, prevState);
      };

      // Define stage 2 function: Return the bulb's color back to prevState when applicable.
      stage2 = (device, prevState) => {
        if (
          activeCycles.some((cyc) => {
            if (cyc.id === args[1][0].id) return false;
            if (cyc.bulbs.indexOf(device) === -1) return false;
            if (cyc.cycle.highPriority && !cycle.highPriority) return true;
            return false;
          })
        ) {
          activeCycles = activeCycles.filter((cyc) => cyc.id !== args[1][0].id);
          mainWindow.webContents.send("log", [
            "console",
            `Main: cycle id ${args[1][0].id} stopped; another high-priority cycle is now running for this bulb.`,
          ]);
          delete prevBulbStates[device];
          return;
        }

        // No need to return to prevState if persisting or alert was not set.
        if (
          cycle.persist ||
          typeof cycle.state.alert === "undefined" ||
          cycle.state.alert === "none"
        ) {
          delete prevBulbStates[device];
          return;
        }

        mainWindow.webContents.send("log", [
          "console",
          `Main -> cycle ${args[1][0].id}: returning to previous state.`,
        ]);

        // Do not repeat flashing
        prevState.alert = "none";

        changeHueBulbState(device, prevState)
          .then((result) => {
            if (result) {
              delete prevBulbStates[device];
            } else {
              mainWindow.webContents.send("log", [
                "warn",
                `Main -> cycle ${args[1][0].id}: Failed to return to previous state on bulb ${device}. Added to backup cycle.`,
              ]);
              sendToBackup(device, 2, prevState);
            }
          })
          .catch((err) => {
            mainWindow.webContents.send("log", [
              "warn",
              `Main -> cycle ${args[1][0].id}: Failed to return to previous state on bulb ${device}. Added to backup cycle.`,
            ]);
            sendToBackup(device, 2, prevState);
          });
      };

      // Begin the process
      args[1][0].bulbs.forEach((bulb) => {
        if (args[1][0].stage && args[1][0].prevState) {
          prevBulbStates[bulb] = args[1][0].prevState;

          mainWindow.webContents.send("log", [
            "console",
            `Main -> cycle ${args[1][0].id}: Is a backup cycle. Starting at stage ${bulb.stage}.`,
          ]);
          switch (args[1][0].stage) {
            case 1:
              stage1(bulb, args[1][0].prevState, cycle);
              break;
            case 2:
              stage2(bulb, args[1][0].prevState);
              break;
          }
        } else {
          let startProcess = (lightState) => {
            if (
              args[1][0].ignoreIfSaturation &&
              lightState.sat >= 5 &&
              lightState.colormode !== "ct"
            ) {
              mainWindow.webContents.send("log", [
                "console",
                `Main -> cycle ${args[1][0].id}: Skipping ${bulb}; ignoreIfSaturation was specified, and bulb had saturation and was not in ct (kelvin) color mode.`,
              ]);
              return;
            }

            if (
              (cycle.state.alert === "select" ||
                cycle.state.alert === "lselect") &&
              (typeof cycle.power === "undefined" || cycle.power === 0) &&
              (typeof cycle.state === "undefined" ||
                typeof cycle.state.on === "undefined" ||
                cycle.state.on === false) &&
              !lightState.on
            ) {
              mainWindow.webContents.send("log", [
                "console",
                `Main -> cycle ${args[1][0].id}: Skipping ${bulb}; we are trying to flash on a bulb that is turned off (and we did not explicitly want it to come on for a flash).`,
              ]);
              return;
            }

            stage1(bulb, lightState, cycle);
          };

          if (typeof prevBulbStates[bulb] === "undefined") {
            getHueBulbState(bulb)
              .then((lightState) => {
                prevBulbStates[bulb] = lightState;
                startProcess(lightState);
              })
              .catch((e) => {
                console.error(e);
                mainWindow.webContents.send("log", [
                  "error",
                  `Main -> cycle ${args[1][0].id}: Bulb ${bulb} had an initial error. Adding to bulb's backup cycle.`,
                ]);
                sendToBackup(bulb, 0, {});
              });
          } else {
            startProcess(Object.assign({}, prevBulbStates[bulb]));
          }
        }
      });

      // Indicate to the renderer when the cycle will be complete.
      setTimeout(
        () => {
          activeCycles = activeCycles.filter((cyc) => cyc.id !== args[1][0].id);
          mainWindow.webContents.send("cycleComplete", [args[1][0].id]);
        },
        delay * (cycle.repeat || 1) + 2000 * (cycle.repeat || 1)
      );

      break;
  }
});

ipcMain.handle("settings", async (event, args) => {
  return store.get(`settings.${args}`);
});

/**
 * Update bulb data in cache.
 */
function updateBulbCache(light) {
  let basicInfo = {
    id: light.id,
    uniqueid: light.uniqueid,
    name: light.name,
    online: light.state.reachable ? 1 : 0,
  };

  // Create a persistent cache of the bulb in settings
  store.set(`bulbs.${light.uniqueid}`, basicInfo);

  // Create a local cache of the bulb in memory
  if (typeof bulbs[light.uniqueid] === "undefined") {
    bulbs[light.uniqueid] = basicInfo;
  } else {
    bulbs[light.uniqueid] = Object.assign(bulbs[light.uniqueid], basicInfo);
  }
}

/**
 * Updates a bulb in the system.
 */
function updateBulb(light) {
  updateBulbCache(light);
  mainWindow.webContents.send("bulb", {
    id: light.id,
    uniqueid: light.uniqueid,
    name: light.name,
    state: light.state,
    online: light.state.reachable ? 1 : 0,
  });
}

/**
 * Check for what brightness bulbs should be set at relative to the current position of the sun
 *
 * @returns {number} Brightness percentile; 0 = no light expected, 1 = full brightness expected.
 */
function brightnessViaSun() {
  const cutoffLow = 0 - Math.PI / 36; // Positions below this value will expect full brightness; Math.PI / 2 is least brightness.
  const cutoffHigh = Math.PI / 12; // Positions above this value will expect no brightness.
  let position = SunCalc.getPosition(
    DateTime.now().toJSDate(),
    store.get("settings.location.lat"),
    store.get("settings.location.lon")
  ).altitude;

  // Less than cutoffLow = full brightness
  if (position < cutoffLow) return 1;

  // Greater than cutoffHigh = no brightness
  if (position > cutoffHigh) return 0;

  // Else, brightness is a percentile.
  let distance = cutoffHigh - cutoffLow;
  return (cutoffHigh - position) / distance;
}

/**
 * Determine what kelvin temperature to set the bulbs at.
 *
 * @returns {number} kelvin
 */
function kelvinViaTime() {
  let kelvinSettings = store.get(`settings.sun.kelvin`);

  if (kelvinSettings.minAtHour < kelvinSettings.maxAtHour) {
    let maxDiff =
      60 * (24 - kelvinSettings.maxAtHour) + 60 * kelvinSettings.minAtHour;

    // We should be at minimum Kelvin
    if (
      DateTime.now().hour < kelvinSettings.maxAtHour &&
      DateTime.now().hour >= kelvinSettings.minAtHour
    )
      return kelvinSettings.min;

    let diff = DateTime.now()
      .startOf("day")
      .plus({ days: 1, hours: kelvinSettings.minAtHour })
      .diff(DateTime.now())
      .as("minutes");

    return parseInt(
      kelvinSettings.min +
        (kelvinSettings.max - kelvinSettings.min) * (diff / maxDiff)
    );
  }

  if (kelvinSettings.minAtHour > kelvinSettings.maxAtHour) {
    let maxDiff = 60 * kelvinSettings.minAtHour - 60 * kelvinSettings.maxAtHour;

    // We should be at minimum Kelvin
    if (
      DateTime.now().hour >= kelvinSettings.minAtHour ||
      DateTime.now().hour < kelvinSettings.maxAtHour
    )
      return kelvinSettings.min;

    let diff = DateTime.now()
      .startOf("day")
      .plus({ hours: kelvinSettings.minAtHour })
      .diff(DateTime.now())
      .as("minutes");

    return parseInt(
      kelvinSettings.min +
        (kelvinSettings.max - kelvinSettings.min) * (diff / maxDiff)
    );
  }

  // Min and max hours are the same; treat as 24 hours
  let maxDiff = 60 * 24;

  let diff = DateTime.now()
    .startOf("day")
    .plus({
      days: DateTime.now().hour < kelvinSettings.minAtHour ? 0 : 1,
      hours: kelvinSettings.minAtHour,
    })
    .diff(DateTime.now())
    .as("minutes");

  return parseInt(
    kelvinSettings.min +
      (kelvinSettings.max - kelvinSettings.min) * (diff / maxDiff)
  );
}

async function oneMinuteTasks() {
  await checkNWSAlerts();
}

/**
 * Check NWS for weather alerts and push to renderer.
 */
async function checkNWSAlerts() {
  // Get active weather alerts
  let weatherAlerts = [];
  try {
    weatherAlerts = await nws.getAlerts(true, {
      latitude: store.get(`settings.location.lat`),
      longitude: store.get(`settings.location.lon`),
    });
    weatherAlerts = weatherAlerts.features.map((feature) => feature.properties);

    cachedWeatherAlerts = weatherAlerts;
    lastWeatherUpdate = DateTime.now().toUnixInteger();
  } catch (e) {
    weatherAlerts = cachedWeatherAlerts;
    console.error(e);
    mainWindow.webContents.send("log", [
      "warn",
      `oneMinuteTasks: NWS encountered an error: ${e.message}. Using cached weather alerts data from previous successful fetch.`,
    ]);
  }

  let activeAlerts = store.get(`weatherAlerts`);
  let nwsAlerts = store.get(`settings.NWS.alerts`);
  
  weatherAlerts.forEach((alert) => {
    // Alert is new and in our list of alerts to cover
    if (
      (!activeAlerts ||
        !activeAlerts.find((alertB) => alertB.id === alert.id)) &&
      nwsAlerts[alert.event]
    ) {
      mainWindow.webContents.send("newWeatherAlert", alert);
    }
  });

  // Remember basic details of active weather alerts
  store.set(
    `weatherAlerts`,
    weatherAlerts.map((alert) => {
      return { id: alert.id, severity: alert.severity, event: alert.event };
    })
  );

  mainWindow.webContents.send("activeWeatherAlerts", weatherAlerts);
}

/**
 * Check Tempest for observations and upcoming forecast, and push to renderer.
 */
async function checkTempest() {
  let response;
  let data;
  const apiURL = `https://swd.weatherflow.com/swd/rest`;
  const endpoints = {
    observations: "/observations/station",
    forecast: "/better_forecast",
  };

  let tempestSettings = store.get("settings.weather.tempest", null);
  if (
    tempestSettings === null ||
    tempestSettings.token === "" ||
    tempestSettings.stationId === ""
  ) {
    mainWindow.webContents.send("log", [
      "warn",
      `Tempest weather: SKIPPED; Tempest access token and station ID not configured.`,
    ]);
    return;
  }

  // Get observations
  try {
    response = await fetch(
      `${apiURL}${endpoints.observations}/${tempestSettings.stationId}?token=${tempestSettings.token}`
    );
    checkStatus(response);

    data = await response.json();
    if (
      typeof data.status === "undefined" ||
      data.status["status_code"] !== 0
    ) {
      let statusCode =
        response.ok === false || typeof response.body.status === "undefined"
          ? -1
          : response.body.status["status_code"];
      throw new Error(
        `HTTP code ${response.statusCode}, status code ${statusCode}. Using cached weather observations.`
      );
    }

    cachedObservations = data["obs"];
  } catch (err) {
    mainWindow.webContents.send("log", [
      "warn",
      `Tempest weather: Error getting observations. ${err.message}`,
    ]);
  }

  // Get current conditions and forecast
  try {
    response = await fetch(
      `${apiURL}${endpoints.forecast}?station_id=${tempestSettings.stationId}&token=${tempestSettings.token}`
    );
    checkStatus(response);

    data = await response.json();
    if (
      typeof data.status === "undefined" ||
      data.status["status_code"] !== 0
    ) {
      let statusCode =
        response.ok === false || typeof response.body.status === "undefined"
          ? -1
          : response.body.status["status_code"];
      throw new Error(
        `HTTP code ${response.statusCode}, status code ${statusCode}. Using cached weather observations.`
      );
    }

    cachedWeather = data["current_conditions"];
    cachedForecast = data["forecast"];
  } catch (err) {
    mainWindow.webContents.send("log", [
      "warn",
      `Tempest weather: Error getting weather / forecast. ${err.message}`,
    ]);
  }
}

async function fiveMinuteTasks() {
  // Check Tempest and update bulbs for weather / sun / clouds / time
  await checkTempest();
  await updateWeather();

  // Request 5-minute weather alert sequence
  mainWindow.webContents.send(
    "weatherAlertsSequence",
    store.get(`weatherAlerts`, [])
  );
}

/**
 * Calculate how bright the lights should be
 * @param {number} x Current outdoor illuminance in lux
 * @returns {number} The brightness at which the bulbs should be set
 */
function calculate(x) {
  let sunBrightnessSettings = store.get(`settings.weather.sunBrightness`);

  if (sunBrightnessSettings.maxLux <= 0) return 0;

  let brightnessDiff =
    sunBrightnessSettings.maxBrightness - sunBrightnessSettings.minBrightness;

  if (x <= sunBrightnessSettings.minLux)
    return sunBrightnessSettings.maxBrightness;
  if (x >= sunBrightnessSettings.maxLux)
    return sunBrightnessSettings.minBrightness;

  x = ((x - sunBrightnessSettings.minLux) / sunBrightnessSettings.maxLux) * 100;

  let calc = 0.0115571 * Math.pow(x, 2) - 2.10493 * x + 95.9381;

  if (calc < 0) return sunBrightnessSettings.minBrightness;
  if (calc > 100) return sunBrightnessSettings.maxBrightness;

  return sunBrightnessSettings.minBrightness + (calc / 100) * brightnessDiff;
}

async function updateWeather() {
  // Get expected sun brightness
  let brightness = brightnessViaSun();
  // Get expected time kelvin
  let kelvin = kelvinViaTime();

  // Adjust brightness by settings
  let brightnessSettings = store.get(`settings.sun.sun`);
  let diff =
    brightnessSettings.maxBrightness - brightnessSettings.minBrightness;
  brightness = brightnessSettings.minBrightness + brightness * diff;

  // Adjust sun brightness by settings
  let illuminance =
    typeof cachedWeather["brightness"] !== "undefined"
      ? cachedWeather["brightness"]
      : 0;
  let sunBrightness = calculate(illuminance);

  // Is lightning in the forecast?
  let lightningForecast =
    typeof cachedWeather["conditions"] !== "undefined" &&
    ["Thunderstorms Likely", "Thunderstorms Possible"].indexOf(
      cachedWeather["conditions"]
    ) !== -1;

  // Do we have lightning?
  let lightningDetected =
    typeof cachedWeather["lightning_strike_last_epoch"] !== "undefined"
      ? DateTime.now().toUnixInteger() <
        cachedWeather["lightning_strike_last_epoch"] +
          store.get(`settings.weather.lightning.maxTime`, 60 * 15)
      : false;
  let lightningTime =
    typeof cachedWeather["lightning_strike_last_epoch"] !== "undefined"
      ? cachedWeather["lightning_strike_last_epoch"]
      : null;

  // Is precipitation in the forecast?
  let precipitationForecast =
    lightningForecast ||
    (typeof cachedWeather["conditions"] !== "undefined" &&
      [
        "Rain Likely",
        "Rain Possible",
        "Snow Likely",
        "Snow Possible",
        "Wintry Mix Likely",
        "Wintry Mix Possible",
      ].indexOf(cachedWeather["conditions"]) !== -1);

  // Do we have precipitation?
  let precipitationDetected = cachedObservations.some(
    (value) => typeof value["precip"] !== "undefined" && value["precip"] > 0
  );
  let precipitationAmount =
    cachedObservations.length < 1
      ? 0
      : cachedObservations
          .map((obs) => obs.precip)
          .reduce((prevValue, curValue) => curValue + (prevValue || 0)) /
        cachedObservations.length;

  // Begin sun process
  mainWindow.webContents.send(
    "processBrightnessKelvinPrecip",
    DateTime.now().toUnixInteger() - lastWeatherUpdate < 60 * 10,
    cachedWeather["conditions"],
    brightness,
    kelvin,
    sunBrightness,
    lightningForecast,
    lightningDetected,
    lightningTime,
    precipitationForecast,
    precipitationDetected,
    precipitationAmount,
    store.get(`settings.sun.bulbsSun`),
    store.get(`settings.sun.bulbsKelvin`),
    store.get(`settings.weather.bulbsSunBrightness`),
    store.get(`settings.weather.bulbsLightning`),
    store.get(`settings.weather.bulbsPrecipitation`)
  );
}

/**
 * Calculate what color to use for a bulb.
 *
 * @param {object} _setColor The state object requested (standard color values)
 * @param {object} _currentColor The current color (state) of the bulb (hue API value)
 * @returns {object|undefined} The color to use (hue API value)
 */
function determineHueColor(_setColor, _currentColor) {
  // Prevent overwriting parameters in place by creating new objects.
  let setColor = Object.assign({}, _setColor);
  let currentColor = Object.assign({}, _currentColor);

  /**
   * Define internal function that evaluates a single color property.
   * @param {"hue" | "sat" | "bri" | "ct"} type
   * @param {number | object<string, number>} obj
   * @returns {number | undefined}
   */
  const evaluateValue = (type, obj) => {
    // Cannot set hue, sat, ct, bri, or xy on a powered-off bulb unless we are powering it on.
    if (
      ["hue", "sat", "ct", "bri", "xy"].indexOf(type) !== -1 &&
      (typeof setColor["on"] === "undefined" || setColor["on"] === false) &&
      currentColor["on"] === false
    )
      return undefined;

    let value = setColor[type];

    if (typeof obj !== "object") {
      value = obj;
    } else if (typeof currentColor[type] !== "undefined") {
      value = hueValueToColor(type, currentColor[type]);

      for (let key in obj) {
        if (!Object.prototype.hasOwnProperty.call(obj, key)) continue;

        // Perform the operations
        switch (key) {
          case "+":
            value += obj[key];
            break;
          case "-":
            value -= obj[key];
            break;
          case "*":
            value *= obj[key];
            break;
          case "/":
            obj[key] !== 0 ? (value /= obj[key]) : (value = value);
            break;
        }
      }
    } else {
      // Cannot modify a property that does not exist in current light states!
      return undefined;
    }

    // Make sure we are within limits
    switch (type) {
      case "hue":
        while (value < 0) {
          value += 360;
        }
        while (value >= 360) {
          value -= 360;
        }
        break;
      case "sat":
      case "bri":
        if (value < 0) value = 0;
        if (value > 100) value = 100;
        break;
      case "ct":
        if (value < 2000) value = 2000;
        if (value > 6500) value = 6500;
        break;
    }

    return value;
  };

  // Loop through each color property
  ["hue", "sat", "bri", "ct"].forEach((type) => {
    if (typeof setColor[type] !== "undefined") {
      setColor[type] = evaluateValue(type, setColor[type]);
    }
    if (typeof setColor[type] !== "undefined") {
      // Could have been set to undefined by evaluateValue
      setColor[type] = colorToHueValue(type, setColor[type]);
    }
  });

  return setColor;
}

/**
 * Convert a standard color value to its Hue API value.
 *
 * @param @param {"hue" | "sat" | "bri" | "ct"} key
 * @param {number} value
 * @returns {number} The Hue API value
 */
function colorToHueValue(key, value) {
  // Make sure we are within limits
  switch (key) {
    case "hue":
      while (value < 0) {
        value += 360;
      }
      while (value >= 360) {
        value -= 360;
      }
      break;
    case "sat":
    case "bri":
      if (value < 0) value = 0;
      if (value > 100) value = 100;
      break;
    case "ct":
      if (value < 2000) value = 2000;
      if (value > 6500) value = 6500;
      break;
  }

  // Make the conversion
  switch (key) {
    case "hue":
      value = (value / 360) * 65535;
      break;
    case "sat":
      value = (value / 100) * 254;
      break;
    case "bri":
      value = (value / 100) * 254;
      if (value < 1) value = 1;
      break;
    case "ct":
      value = (1 - (value - 2000) / (6500 - 2000)) * (500 - 153) + 153;
      break;
  }

  return value;
}

/**
 * Convert a Hue API value to a standard color value
 *
 * @param @param {"hue" | "sat" | "bri" | "ct"} key
 * @param {number} value
 * @returns {number} The standard color value
 */
function hueValueToColor(key, value) {
  // Make sure we are within limits
  switch (key) {
    case "hue":
      while (value < 0) {
        value += 65535;
      }
      while (value >= 65535) {
        value -= 65535;
      }
      break;
    case "sat":
      if (value < 0) value = 0;
      if (value > 254) value = 254;
      break;
    case "bri":
      if (value < 1) value = 1;
      if (value > 254) value = 254;
      break;
    case "ct":
      if (value < 153) value = 153;
      if (value > 500) value = 500;
      break;
  }

  // Make the conversion
  switch (key) {
    case "hue":
      value = (value / 65535) * 360;
      break;
    case "sat":
    case "bri":
      value = (value / 254) * 100;
      break;
    case "ct":
      value = (1 - (value - 153) / (500 - 153)) * (6500 - 2000) + 2000;
      break;
  }

  return value;
}

async function discoverBridge() {
  let ipAddress = "";

  try {
    ipAddress = await discoverBridgeMdns();
    if (ipAddress === "") {
      ipAddress = await discoverBridgeNupnp();
    }
  } catch (err) {
    mainWindow.webContents.send("log", [
      "error",
      `Philips Hue: Cannot discover bulbs: ${err.message}.`,
    ]);
  }

  store.set("settings.hue.ipAddress", ipAddress);

  if (ipAddress === "") {
    mainWindow.webContents.send("log", [
      "error",
      `Philips Hue: No bridges found! The IP Address setting has been blanked. See the Debug page for more information.`,
      true,
    ]);
  } else {
    mainWindow.webContents.send("log", [
      "success",
      `Philips Hue: Your bridge was found at the IP address ${ipAddress}. It has been set in your Hue Settings.`,
      true,
    ]);
  }

  return ipAddress;
}

async function discoverBridgeNupnp() {
  mainWindow.webContents.send("log", [
    "log",
    `Philips Hue: Discovering bridge via meethue...`,
  ]);
  const discoveryResults = await nodeHue.discovery.nupnpSearch();

  if (discoveryResults.length === 0) {
    mainWindow.webContents.send("log", [
      "warn",
      `Philips Hue: No bridges found via meethue.`,
    ]);
    return "";
  } else {
    mainWindow.webContents.send("log", [
      "log",
      `Philips Hue: Bridge found via meethue.`,
    ]);
    // Ignoring that you could have more than one Hue Bridge on a network as this is unlikely in 99.9% of users situations
    return discoveryResults[0].ipaddress;
  }
}

async function discoverBridgeMdns() {
  mainWindow.webContents.send("log", [
    "log",
    `Philips Hue: Discovering bridge via MDNS...`,
  ]);
  const discoveryResults = await nodeHue.discovery.mdnsSearch(10000);

  if (
    discoveryResults.length === 0 ||
    typeof discoveryResults[0].ipaddress === "undefined"
  ) {
    mainWindow.webContents.send("log", [
      "warn",
      `Philips Hue: No bridges found via MDNS.`,
    ]);
    return "";
  } else {
    mainWindow.webContents.send("log", [
      "log",
      `Philips Hue: Bridge found via MDNS.`,
    ]);
    // Ignoring that you could have more than one Hue Bridge on a network as this is unlikely in 99.9% of users situations
    return discoveryResults[0].ipaddress;
  }
}

async function createHueUser() {
  if (store.get("settings.hue.username", "") !== "") {
    mainWindow.webContents.send("log", [
      "error",
      `Philips Hue: Could not create user; a username is already specified in the settings. Please erase the current username from the Hue settings if you must create a new user.`,
      true,
    ]);
    return;
  }

  let ipAddress = store.get("settings.hue.ipAddress", "");

  if (ipAddress === "") {
    mainWindow.webContents.send("log", [
      "error",
      `Philips Hue: Could not create user; IP address of the bridge is not specified. You might want to go in the Required page and run the Find Bridge IP Address tool.`,
      true,
    ]);
    return;
  }

  try {
    // Create an unauthenticated instance of the Hue API so that we can create a new user
    mainWindow.webContents.send("log", [
      "info",
      "Philips Hue: Connecting locally to the bridge...",
      true,
    ]);
    const unauthenticatedApi = await nodeHue.api
      .createLocal(ipAddress)
      .connect();

    mainWindow.webContents.send("log", [
      "info",
      "Philips Hue: Connected locally to the bridge. Now attempting to create a new user on the bridge...",
      true,
    ]);
    let createdUser;
    createdUser = await unauthenticatedApi.users.createUser(
      appName,
      deviceName
    );
    mainWindow.webContents.send("log", [
      "info",
      "Philips Hue: User has been created on the Hue Bridge. The following username can be used to\n" +
        "authenticate with the Bridge and provide full local access to the Hue Bridge.\n" +
        "YOU SHOULD TREAT THIS LIKE A PASSWORD\n" +
        `Hue Bridge User: ${createdUser.username}` +
        "\n" +
        `Hue Bridge User Client Key: ${createdUser.clientkey}`,
      true,
    ]);

    // Create a new API instance that is authenticated with the new user we created
    const authenticatedApi = await nodeHue.api
      .createLocal(ipAddress)
      .connect(createdUser.username);

    // Do something with the authenticated user/api
    const bridgeConfig =
      await authenticatedApi.configuration.getConfiguration();

    store.set("settings.hue.username", createdUser.username);
    store.set("settings.hue.clientKey", createdUser.clientkey);

    mainWindow.webContents.send("log", [
      "success",
      "Philips Hue: successfully authenticated with the new username and client key.",
      true,
    ]);
  } catch (err) {
    mainWindow.webContents.send("log", [
      "error",
      `Philips Hue: Error while creating local user: ${err.message}`,
      true,
    ]);
  }
}

async function connectToHue(firstConnection = false) {
  let api;
  let useRemote = false;

  let remoteApi = await connectToHueRemote(firstConnection);
  await checkHueRemoteTokens();

  let ipAddress = store.get("settings.hue.ipAddress", "");
  if (ipAddress === "") {
    useRemote = true;
  }
  const username = store.get("settings.hue.username", "");
  if (username === "") {
    useRemote = true;
  }

  if (!useRemote) {
    try {
      api = await nodeHue.api.createLocal(ipAddress).connect(username);
      return api;
    } catch (error) {
      if (remoteApi !== null) {
        return remoteApi;
      }
      throw new Error(
        `Could not connect to hue bridge locally or remotely. You may wish to run / re-run the setup tools on the Required page if you have confirmed your network is stable.`
      );
    }
  } else if (remoteApi === null) {
    throw new Error(
      `Could not connect to hue bridge locally or remotely. You may wish to run / re-run the setup tools on the Required page if you have confirmed your network is stable.`
    );
  }

  return remoteApi;
}

async function connectToHueRemote(firstConnection) {
  // Some other part of the code explicitly said we do not want to try using the remote API
  if (nodeHueRemoteApi === false) {
    return null;
  }

  if (typeof nodeHueRemoteApi !== "undefined") {
    return nodeHueRemoteApi;
  }

  let oauth = store.get("oAuth.hue", {});
  if (
    typeof oauth.accessToken === "undefined" ||
    typeof oauth.refreshToken === "undefined"
  ) {
    return null;
  }

  try {
    nodeHueRemoteApi = await nodeHueRemote.connectWithTokens(
      oauth.accessToken.value,
      oauth.refreshToken.value,
      oauth.username
    );

    if (firstConnection) await refreshRemoteHueTokens();

    return nodeHueRemoteApi;
  } catch (error) {
    if (firstConnection === true) {
      nodeHueRemoteApi = false;
    } else {
      nodeHueRemoteApi = undefined;
    }
    mainWindow.webContents.send("log", [
      "warn",
      `Philips Hue: Cannot connect to bridge remotely: ${error.message}`,
    ]);
    return null;
  }
}

async function discoverHueBulbs(firstConnection) {
  return await mutex.runExclusive(async () => {
    try {
      const api = await connectToHue(firstConnection);
      const lights = await api.lights.getAll();
      lights.forEach((light) => {
        updateBulb(light);
      });
    } catch (error) {
      if (
        firstConnection === true &&
        typeof nodeHueRemoteApi !== "undefined" &&
        nodeHueRemoteApi !== false
      ) {
        if (error.message.includes("Invalid Access Token")) {
          nodeHueRemoteApi = false;
          mainWindow.webContents.send("log", [
            "warn",
            `Philips Hue: Invalid remote access tokens. Remote API disabled this session, and only local connections will be used.`,
          ]);
          discoverHueBulbs(firstConnection);
        }
      }
      mainWindow.webContents.send("log", [
        "error",
        `Philips Hue: cannot discover hue bulbs: ${error.message}`,
      ]);
    }
  });
}

async function changeHueBulbState(uniqueid, state) {
  return await mutex.runExclusive(async () => {
    try {
      if (typeof bulbs[uniqueid] === "undefined") {
        mainWindow.webContents.send("log", [
          "error",
          `Philips Hue changeHueBulbState: Bulb unique id ${uniqueid} not found.`,
        ]);
        return true; // We do not want this being added to the backup cycle
      }
      if (typeof bulbs[uniqueid].online === -1) {
        mainWindow.webContents.send("log", [
          "error",
          `Philips Hue changeHueBulbState: Bulb unique id ${uniqueid} not registered on the configured Bridge.`,
        ]);
        throw new Error(
          `Bulb unique id ${uniqueid} not registered on the configured Bridge.`
        );
      }

      // Hue does not support passing in explicit undefined or null values. Get rid of these.
      for (let key in state) {
        if (!Object.prototype.hasOwnProperty.call(state, key)) continue;

        if (
          typeof state[key] === "undefined" ||
          state[key] === null ||
          state[key] === undefined
        )
          delete state[key];
      }

      if (typeof state.colormode === "undefined") {
        if (
          typeof state.hue !== "undefined" ||
          (typeof state.sat !== "undefined" && state.sat > 0)
        ) {
          state.colormode = "hs";
        } else if (typeof state.xy !== "undefined") {
          state.colormode = "xy";
        } else if (typeof state.ct !== "undefined") {
          state.colormode = "ct";
        }
      }

      if (
        (typeof state.colormode !== "undefined" ||
          (typeof state.alert !== "undefined" && state.alert !== "none")) &&
        ((typeof state.on === "undefined" || state.on === false) &&
          (typeof prevBulbStates[uniqueid] !== "undefined" && prevBulbStates[uniqueid].on === false))
      ) {
        mainWindow.webContents.send("log", [
          "warn",
          `Philips Hue changeHueBulbState: skipped bulb ${uniqueid}; the bulb is turned off, and we are not powering it on.`,
        ]);
        console.dir(state);
        console.dir(prevBulbStates[uniqueid]);
        return true; // We do not want it going into the backup cycles
      }

      const api = await connectToHue();
      const bulbId = bulbs[uniqueid].id;
      const result = await api.lights.setLightState(bulbId, state);
      return result;
    } catch (error) {
      mainWindow.webContents.send("log", [
        "error",
        `Philips Hue: cannot change hue bulb state for ${uniqueid}: ${error.message}`,
      ]);
      throw new Error(
        `Philips Hue: cannot change hue bulb state for ${uniqueid}: ${error.message}`
      );
    }
  });
}

async function getHueBulbState(uniqueid) {
  return await mutex.runExclusive(async () => {
    try {
      if (typeof bulbs[uniqueid] === "undefined") {
        throw new Error(`Bulb unique id ${uniqueid} not found.`);
      }
      if (typeof bulbs[uniqueid].online === -1) {
        throw new Error(
          `Bulb unique id ${uniqueid} not registered on the configured Bridge.`
        );
      }

      // We have a cached previous state we want to use instead (to avoid getting a state in the middle of a cycle)
      if (typeof prevBulbStates[uniqueid] !== "undefined") {
        return prevBulbStates[uniqueid];
      }

      const api = await connectToHue();
      const bulbId = bulbs[uniqueid].id;
      const result = await api.lights.getLightState(bulbId);
      return result;
    } catch (error) {
      mainWindow.webContents.send("log", [
        "error",
        `Philips Hue: cannot fetch hue bulb state: ${error.message}`,
      ]);
      throw new Error(
        `Philips Hue: cannot fetch hue bulb state: ${error.message}`
      );
    }
  });
}

class HTTPResponseError extends Error {
  constructor(response) {
    super(`HTTP Error Response: ${response.status} ${response.statusText}`);
    this.response = response;
  }
}

const checkStatus = (response) => {
  if (response.ok) {
    // response.status >= 200 && response.status < 300
    return response;
  } else {
    throw new HTTPResponseError(response);
  }
};

async function refreshRemoteHueTokens() {
  if (typeof nodeHueRemoteApi === "undefined") {
    return;
  }

  // Explicitly disabled
  if (nodeHueRemoteApi === false) {
    return;
  }

  try {
    await nodeHueRemoteApi.remote.refreshTokens();
    let config = nodeHueRemoteApi.remote.getRemoteAccessCredentials();
    if (
      typeof config === "undefined" ||
      typeof config.tokens === "undefined" ||
      typeof config.tokens.access === "undefined" ||
      typeof config.tokens.refresh === "undefined" ||
      typeof config.username === "undefined"
    ) {
      nodeHueRemoteApi = false;
      mainWindow.webContents.send("log", [
        "error",
        `Philips Hue: Failed to refresh remote access token: API did not return a new token. Remote API has been disabled for this session, and we will only use local connection if configured.`,
        true,
      ]);
      return;
    }
    store.set("oAuth.hue", {
      accessToken: config.tokens.access,
      refreshToken: config.tokens.refresh,
      username: config.username,
    });
    nodeHueRemoteRefresh = DateTime.now().toUnixInteger();
    mainWindow.webContents.send("log", [
      "info",
      "Philips Hue: remote access token was refreshed.",
    ]);
  } catch (error) {
    nodeHueRemoteApi = undefined;
    mainWindow.webContents.send("log", [
      "error",
      `Philips Hue: Failed to refresh remote access token: ${error.message}.`,
      true,
    ]);
  }
}

async function checkHueRemoteTokens() {
  let oauth = store.get("oAuth.hue", {});

  if (
    typeof nodeHueRemoteApi === "undefined" ||
    typeof oauth.accessToken === "undefined" ||
    typeof oauth.refreshToken === "undefined"
  ) {
    mainWindow.webContents.send("log", [
      "warn",
      "Philips Hue: Remote API not configured. We will only be using local connections to your bridge.",
      true,
    ]);
    return;
  }

  // Is our refresh token expired? (cannot refresh our tokens; log out of the remote API)
  if (DateTime.now().toUnixInteger() > oauth.refreshToken.expiresAt) {
    store.set("oAuth.hue", {});
    nodeHueRemoteApi = false;
    mainWindow.webContents.send("log", [
      "warn",
      "Philips Hue: Remote authorization expired. Please re-authorize this app in the Required page and the Authorize Remote API tool. We will only be using local connections to your bridge.",
      true,
    ]);
    return;
  }

  // Refresh tokens if accessToken is set to expire in the next 72 hours
  if (
    typeof nodeHueRemoteApi !== "undefined" &&
    nodeHueRemoteApi !== false &&
    DateTime.now().toUnixInteger() > oauth.accessToken.expiresAt - 60 * 60 * 72
  ) {
    await refreshRemoteHueTokens();
  }
}
