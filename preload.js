"use strict";
// All of the Node.js APIs are available in the preload process.
// It has the same sandbox as a Chrome extension.
const { ipcRenderer, contextBridge } = require("electron");

contextBridge.exposeInMainWorld("ipc", {
  // main worker
  main: {
    send: (task, args) => ipcRenderer.send("main", [task, args]),
    sendSync: (task, args) => {
      return ipcRenderer.sendSync("main", [task, args]);
    },
    on: (event, fn) => ipcRenderer.on(event, fn),
  },

  settings: (key) => {
    return ipcRenderer.invoke("settings", key);
  },
});
