"use strict";

// Declare variables
const logger = new Logger("#app-log-box");
const appNavigation = new HueNavigation();
const appManager = new HueManager(logger);
const appSettings = new HueSettings(appManager);
const appSchedule = new HueScheduleManager(appManager, appSettings, logger);

// Add unload event to confirm closing the app
let forceClose = false;
window.addEventListener("beforeunload", function (e) {
  if (forceClose) return;

  e.preventDefault();
  e.returnValue = "";
  confirmDialog(
    `Are you sure you want to close the application?
  
<strong>None of the features (eg. sun / cloud brightness, precipitation, weather alerts, schedules, kelvin) will work when the app is closed.</strong>`,
    null,
    () => {
      forceClose = true;
      window.close();
    }
  );
});

// Determine when receiving the settings from main was the first time done.
let firstTimeSettings = true;

// set later to use local time
later.date.localTime();

let currentPrecipitation = false;
let forecastPrecipitation = false;
let currentLightning = false;
let forecastLightning = false;
let previouslyLightning = false;
let previouslyPrecipitation = false;
let currentConnected = true;

// Define navigation
appNavigation.addItem("#nav-lights", "#section-lights", "Lights", "/", true);
appNavigation.addItem("#nav-log", "#section-log", "Debug / Log", "/log");
appNavigation.addItem(
  "#nav-weather-observations",
  "#section-weather-observations",
  "Weather Observations",
  "/observations"
);

appNavigation.addItem(
  "#nav-settings-required",
  "#section-settings-required",
  "Required Settings",
  "/settings/required"
);
appNavigation.addItem(
  "#nav-settings-sun",
  "#section-settings-sun",
  "Sun Settings",
  "/settings/sun"
);
appNavigation.addItem(
  "#nav-settings-weather",
  "#section-settings-weather",
  "Weather Settings",
  "/settings/weather"
);
appNavigation.addItem(
  "#nav-settings-alerts",
  "#section-settings-alerts",
  "NWS Alerts Settings",
  "/settings/alerts"
);
appNavigation.addItem(
  "#nav-settings-schedules",
  "#section-settings-schedules",
  "Schedules",
  "/settings/schedules"
);

// Settings click events
$("#section-settings-required-location").on("click", () => {
  appSettings.showLocationSettings();
});
$("#section-settings-required-hue").on("click", () => {
  appSettings.showHueSettings();
});
$("#section-settings-required-hue-create-user").on("click", () => {
  window.ipc.main.send("create-user", []);
});
$("#section-settings-required-hue-authorize").on("click", () => {
  window.ipc.main.send("oauth-hue", []);
});
$("#section-settings-required-hue-discover").on("click", () => {
  window.ipc.main.send("discover", []);
});
$("#section-settings-sun-sun").on("click", () => {
  appSettings.showSunSettings();
});
$("#section-settings-sun-kelvin").on("click", () => {
  appSettings.showKelvinSettings();
});
$("#section-settings-sun-sun-bulbs").on("click", () => {
  appSettings.showSunBulbSettings();
});
$("#section-settings-sun-sun-kelvin").on("click", () => {
  appSettings.showKelvinBulbSettings();
});
$("#section-settings-weather-tempest").on("click", () => {
  appSettings.showTempestSettings();
});
$("#section-settings-weather-sunbrightness").on("click", () => {
  appSettings.showSunBrightnessSettings();
});
$("#section-settings-weather-sunbrightness-bulbs").on("click", () => {
  appSettings.showSunBrightnessBulbSettings();
});
$("#section-settings-weather-precipitation-bulbs").on("click", () => {
  appSettings.showPrecipitationBulbSettings();
});
$("#section-settings-weather-lightning-bulbs").on("click", () => {
  appSettings.showLightningBulbSettings();
});
$("#section-settings-alerts-alerts").on("click", () => {
  appSettings.showWeatherAlertsSettings();
});
$("#section-settings-alerts-bulbs").on("click", () => {
  appSettings.showWeatherAlertsBulbSettings();
});
$("#btn-schedules-new-state").on("click", () => {
  appSchedule.form_state();
});

// Let main know we are loaded and get initial bulb data
window.ipc.main.send("renderer-ready", []);

// Define IPC listeners
window.ipc.main.on("settings", (event, settingsReceived) => {
  appSettings.newSettings(settingsReceived);
  if (firstTimeSettings) {
    firstTimeSettings = false;
    appSchedule.init(`#section-settings-schedules-table`);
  }
});
window.ipc.main.on("bulbs", (event, bulbs) => {
  appManager.processBulbs(bulbs);
});
window.ipc.main.on("bulb", (event, bulb) => {
  appManager.processBulb(bulb);
  appManager.cyclesReadyToRun = true;
});
window.ipc.main.on("backupCycle", (event, backupCycle) => {
  appManager.processBackupCycleData(
    backupCycle.map((cycle) => new HueCycle(cycle))
  );
});
window.ipc.main.on("cycleComplete", (event, id) => {
  console.log(`Cycle ${id} completed.`);
  appManager.cycleComplete(id);
});
window.ipc.main.on("returnColor", (event, data) => {
  appManager.processReturnColor(data);
});
window.ipc.main.on("log", (event, arg) => {
  console.dir(arg);
  switch (arg[0]) {
    case "console":
      console.log(arg[1]);
      break;
    case "log":
      console.log(arg[1]);
      logger.log(arg[1]);
      break;
    case "info":
      console.info(arg[1]);
      logger.info(arg[1], arg[2]);
      break;
    case "warn":
      console.warn(arg[1]);
      logger.warn(arg[1], arg[2]);
      break;
    case "error":
      console.error(arg[1]);
      logger.error(arg[1], arg[2]);
      break;
    case "success":
      console.log(arg[1]);
      logger.success(arg[1], arg[2]);
      break;
    case "dir":
      console.dir(arg[1]);
      break;
  }
});

window.ipc.main.on(
  "processBrightnessKelvinPrecip",
  (
    event,
    connected,
    conditions,
    brightness,
    kelvin,
    sunBrightness,
    lightningForecast,
    lightningDetected,
    lightningTime,
    precipitationForecast,
    precipitationDetected,
    precipitationAmount,
    bulbsSun,
    bulbsKelvin,
    bulbsSunBrightness,
    bulbsLightning,
    bulbsPrecipitation
  ) => {
    console.log(`processBrightnessKelvinPrecip: Requested.`);

    currentPrecipitation = precipitationDetected;
    forecastPrecipitation = precipitationForecast;
    currentLightning = lightningDetected;
    forecastLightning = lightningForecast;
    currentConnected = connected;

    $(".progress-bar-sun").css("width", `${brightness}%`);
    $(".progress-bar-sunbrightness").css("width", `${sunBrightness}%`);
    $(".value-sun-kelvin").html(kelvin);
    $(".section-settings-weather-status-lightning").html(
      lightningDetected
        ? `DETECTED; last strike was ${moment
            .duration(moment().unix() - lightningTime, "seconds")
            .format("h [hours] m [minutes]")} ago`
        : lightningForecast
        ? "FORECAST SOON"
        : "Not in the near-term forecast"
    );
    $(".section-settings-weather-status-precipitation").html(
      precipitationDetected
        ? `DETECTED; ${precipitationAmount} mm / hour`
        : precipitationForecast
        ? "FORECAST SOON"
        : "Not in the near-term forecast"
    );

    let bulbs = bulbsSun
      .concat(bulbsKelvin)
      .concat(bulbsSunBrightness)
      .concat(bulbsLightning)
      .concat(bulbsPrecipitation);

    if (!bulbs || !bulbs.length) {
      console.info(
        `processBrightnessKelvinPrecip: SKIPPED: no bulbs configured.`
      );
      return;
    }

    // We need to remove bulbs whose color is not hue 0 and saturation 0 (white) and ensure we do not have duplicate bulbs
    let actuallyProcess = [];

    bulbs.forEach((bulb) => {
      if (actuallyProcess.find((bulb2) => bulb2.uniqueid === bulb)) return; // Avoid duplicate bulbs

      // Skip bulbs not in the settings or do not have state data
      let hueBulb = appManager.bulbs[bulb];
      if (!hueBulb || !hueBulb.state || hueBulb.online === -1) {
        console.warn(
          `processBrightnessKelvinPrecip: Skipping bulb ${bulb}; no state info available.`
        );
        logger.warn(
          `processBrightnessKelvinPrecip: Bulb ${bulb} skipped; no state data available for bulb. Maybe it is offline?`
        );
        return;
      }

      actuallyProcess.push(hueBulb);
    });

    if (!actuallyProcess.length) {
      console.warn(
        `processBrightnessKelvinPrecip: SKIPPED: All configured bulbs are unavailable.`
      );
      return;
    }

    console.log(
      `processBrightnessKelvinPrecip: RUNNING on bulbs ${actuallyProcess
        .map((bulb) => bulb.uniqueid)
        .join(", ")}.`
    );
    console.log(
      `processBrightnessKelvinPrecip: Sun Position Brightness: ${brightness}; kelvin: ${kelvin}; sun brightness brightness: ${sunBrightness}; lightning detected: ${lightningTime}; precipitation?: ${precipitationDetected}.`
    );
    logger.info(
      `processBrightnessKelvinPrecip: RUNNING on bulbs ${actuallyProcess
        .map((bulb) => bulb.uniqueid)
        .join(
          ", "
        )}. Sun position brightness: ${brightness}. Kelvin: ${kelvin}. sun brightness brightness: ${sunBrightness}. lightning detected: ${lightningTime}. precipitation?: ${precipitationDetected}.`
    );

    // Process bulb cycles
    let _hue;
    actuallyProcess.forEach((bulb) => {
      // Determine for what this bulb should be adjusted
      let adjustKelvin = bulbsKelvin.indexOf(bulb.uniqueid) !== -1;
      let adjustSun = bulbsSun.indexOf(bulb.uniqueid) !== -1;
      let adjustSunBrightness =
        bulbsSunBrightness.indexOf(bulb.uniqueid) !== -1;
      let adjustLightning = bulbsLightning.indexOf(bulb.uniqueid) !== -1;
      let adjustPrecipitation =
        bulbsPrecipitation.indexOf(bulb.uniqueid) !== -1;

      let adjustingForLightningPrecipitation = false;
      let noMoreLightningPrecip = false;

      // Determine what brightness we should actually use for the bulb
      let _brightness = 0;
      let adjustBrightness = false;

      _hue = undefined;

      // Conditions which would require us to adjust bulb brightness
      if (
        adjustSun ||
        adjustSunBrightness ||
        (adjustLightning && (lightningDetected || lightningForecast)) ||
        (adjustPrecipitation &&
          (precipitationDetected || precipitationForecast))
      ) {
        adjustBrightness = true;

        // Sun position and brightness brightness
        if (adjustSun && adjustSunBrightness) {
          _brightness = (brightness + sunBrightness) / 2;
        } else if (adjustSun) {
          _brightness = brightness;
        } else if (adjustSunBrightness) {
          _brightness = sunBrightness;
        }

        // Precipitation and lightning brightness (and hue)
        if (
          adjustLightning &&
          (lightningDetected ||
            (lightningForecast &&
              (!precipitationDetected || !adjustPrecipitation)))
        ) {
          _brightness = 75 + _brightness / 4; // Brightness should never go below 75 when lightning is detected or in the forecast
          if (lightningDetected) _brightness = 100; // Force 100 brightness when lightning detected
          adjustingForLightningPrecipitation = true;
          _hue = 60; // Yellow
        } else if (
          adjustPrecipitation &&
          (precipitationDetected || precipitationForecast)
        ) {
          // Brightness should never go below 75 when precipitation is detected or in the forecast
          _brightness = 75 + _brightness / 4;
          adjustingForLightningPrecipitation = true;
          _hue = 210; // Cyan
        }
      }

      // Precipitation / lightning previously detected or forecast but no longer
      if (
        (adjustLightning &&
          (!adjustPrecipitation ||
            (!precipitationDetected && !precipitationForecast)) &&
          !lightningDetected &&
          !lightningForecast &&
          previouslyLightning) ||
        (adjustPrecipitation &&
          (!adjustLightning || (!lightningDetected && !lightningForecast)) &&
          !precipitationDetected &&
          !precipitationForecast &&
          previouslyPrecipitation)
      ) {
        adjustingForLightningPrecipitation = true;
        noMoreLightningPrecip = true;
      }

      // Normalize final brightness by preventing the bulb from adjusting any more than 20 brightness per cycle to help reduce drastic changes.
      let prevBrightness =
        bulb.state && bulb.state.bri
          ? appManager.hueValueToColor("bri", bulb.state.bri)
          : _brightness;
      if (Math.abs(_brightness - prevBrightness) > 20) {
        _brightness =
          _brightness < prevBrightness
            ? prevBrightness - 20
            : prevBrightness + 20;
      }

      // Use 100% brightness on lightning / precipitation bulbs which do not adjust for sun brightness
      if (!adjustSun && !adjustSunBrightness) {
        if (
          lightningDetected ||
          precipitationDetected ||
          lightningForecast ||
          precipitationForecast
        ) {
          _brightness = 100;
        } else {
          _brightness = undefined;
        }
      }

      // Add the cycle
      if (noMoreLightningPrecip) {
        appManager.addReturnColorCycle([bulb]);
      }
      appManager.addCycle(
        new HueCycle({
          bulbs: [bulb.uniqueid],
          task: "state",
          backupTolerance: adjustingForLightningPrecipitation ? 15 : 5,
          ignoreIfSaturation: adjustingForLightningPrecipitation ? false : true,
          cycle: {
            persist: true,
            setAsReturnColor: adjustingForLightningPrecipitation ? 0 : 1,
            state: {
              alert: "none",
              on: adjustLightning
                ? lightningDetected
                  ? true
                  : previouslyLightning
                  ? _brightness > 0 || !adjustBrightness
                    ? undefined
                    : false
                  : undefined
                : undefined,
              hue: _hue,
              sat:
                typeof _hue !== "undefined"
                  ? lightningDetected || precipitationDetected
                    ? 66
                    : 33
                  : undefined,
              bri: adjustBrightness ? _brightness : undefined,
              ct:
                adjustKelvin && typeof _hue === "undefined"
                  ? kelvin
                  : undefined,
              colormode:
                typeof _hue !== "undefined"
                  ? "hs"
                  : adjustKelvin
                  ? "ct"
                  : undefined,
            },
          },
        })
      );
    });

    previouslyLightning = lightningDetected || lightningForecast;
    previouslyPrecipitation = precipitationDetected || precipitationForecast;
  }
);

window.ipc.main.on("newWeatherAlert", (event, alert) => {
  let color = appSettings.AlertColors[alert.event];

  console.log(
    `newWeatherAlert: Requested... ${alert.severity} / ${alert.event}.`
  );

  let bulbs = appSettings.settings.NWS.bulbs[alert.severity];

  // Exit if there are no bulbs to trigger
  if (typeof bulbs === "undefined" || bulbs.length === 0) {
    console.info(
      `newWeatherAlert: SKIPPED; no configured bulbs for the alert severity.`
    );
    return;
  }

  console.log(`newWeatherAlert: RUNNING on bulbs ${bulbs.join(", ")}.`);
  logger.info(
    `newWeatherAlert: RUNNING for ${alert.severity} alert ${alert.event}, id ${
      alert.id
    }. Bulbs triggered: ${bulbs.join(", ")}.`
  );

  appManager.addCycle(
    new HueCycle({
      bulbs: bulbs,
      task: "state",
      backupTolerance: 15,
      cycle: {
        highPriority: true,
        persist: false,
        repeat: 4,
        state: Object.assign(appManager.hexToColor(color), {
          on:
            alert.severity === "Extreme" || alert.severity === "Severe"
              ? true
              : undefined,
          alert: "lselect",
        }),
      },
    })
  );
});

window.ipc.main.on("activeWeatherAlerts", (event, alerts) => {
  let html = ``;

  let triggerExtremeReminder = false;
  alerts.forEach((alert) => {
    let color = appSettings.AlertColors[alert.event];
    if (!color) return;

    // Determine if we should pulse bulbs every minute
    if (
      alert.severity === "Extreme" &&
      (alert.urgency === "Immediate" || alert.certainty === "Observed")
    ) {
      triggerExtremeReminder = true;
    }

    html += `<div class="col-12 col-md-6 col-xl-4">
      <span class="badge" style="background: ${color};">${alert.event}</span>
    </div>`;
  });
  $(".active-weather-alerts").html(html);

  if (triggerExtremeReminder && appSettings.settings.NWS.bulbs.Extreme.length) {
    console.log(
      `activeWeatherAlerts: Extreme alert in effect! Running minutely flash cycle on bulbs ${appSettings.settings.NWS.bulbs.Extreme.join(
        ", "
      )}.`
    );
    logger.info(
      `activeWeatherAlerts: Extreme alert in effect! Running minutely flash cycle on bulbs ${appSettings.settings.NWS.bulbs.Extreme.join(
        ", "
      )}.`
    );

    appManager.addCycle(
      new HueCycle({
        bulbs: appSettings.settings.NWS.bulbs.Extreme,
        task: "state",
        backupTolerance: 1,
        cycle: {
          highPriority: true,
          persist: false,
          state: Object.assign(appManager.hexToColor("#ff0000"), {
            on: true,
            alert: "lselect",
          }),
        },
      })
    );
  }
});

window.ipc.main.on("weatherAlertsSequence", (event, alerts) => {
  console.log(`weatherAlertsSequence: requested.`);

  // No alerts? Bail immediately.
  if (alerts.length === 0) {
    console.info(`weatherAlertsSequence: SKIPPED; no active alerts.`);
    return;
  }

  // No bulbs? Also bail.
  if (!appSettings.settings.NWS.bulbs.sequence.length) {
    console.info(`weatherAlertsSequence: SKIPPED; no configured bulbs.`);
    return;
  }

  logger.info(
    `weatherAlertsSequence: RUNNING on bulbs ${appSettings.settings.NWS.bulbs.sequence.join(
      ", "
    )}.`
  );

  let cycles = {
    Extreme: "select",
    Severe: "select",
    Moderate: "select",
    Minor: "select",
  };

  alerts.forEach((alert) => {
    let color = appSettings.AlertColors[alert.event];
    if (!color) return;

    // Grab bulbs set for this alert severity
    let bulbs = appSettings.settings.NWS.bulbs[alert.severity];

    // Exit if there are no bulbs to trigger
    if (typeof bulbs === "undefined" || bulbs.length === 0) {
      return;
    }

    // Filter by bulbs which are also set for the 5-minute cycle
    bulbs = bulbs.filter((bulb) => {
      return appSettings.settings.NWS.bulbs.sequence.includes(bulb);
    });

    // Exit if there are no bulbs to trigger
    if (typeof bulbs === "undefined" || bulbs.length === 0) {
      return;
    }

    // Run the cycle
    appManager.addCycle(
      new HueCycle({
        bulbs: bulbs,
        task: "state",
        backupTolerance: 5,
        cycle: {
          highPriority: true,
          persist: false,
          state: Object.assign(appManager.hexToColor(color), {
            alert: cycles[alert.severity] || "select",
          }),
        },
      })
    );
  });
});
