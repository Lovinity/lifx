/**
 * Class to manage schedules for Hue bulbs and operations.
 * @requires later breejs/later
 * @requires $ jQuery
 * @requires Modal
 */
class HueScheduleManager {
  /**
   * Construct a schedule manager.
   * @param {HueManager} manager The HueManager managing bulbs and cycles.
   * @param {HueSettings} settings The HueSettings managing settings.
   * @param {Logger} logger The Logger being used.
   */
  constructor(manager, settings, logger) {
    this.manager = manager;
    this.settings = settings;
    this.logger = logger;

    /**
     * Array of saved schedules.
     * @type HueSchedule[]
     */
    this.schedules = [];

    /**
     * The datatable listing active schedules.
     */
    this.table;

    /**
     * Main modal for managing schedule settings.
     * @type Modal
     */
    this.modal = new Modal(`Schedule Settings`, ``, ``, true, {
      overlayClose: false,
    });
  }

  /**
   * Add a new LIFX schedule. Takes care to ensure no two schedules with the same id are added.
   * @param {HueSchedule} schedule The schedule to add
   * @returns {boolean} True if the schedule was added.
   */
  addSchedule(schedule) {
    if (this.schedules.find((sch) => sch.id === schedule.id)) {
      schedule.destroy();
      console.warn(`Cannot add schedule ${schedule.id}. It was already added.`);
      this.logger.warn(
        `Schedule ${schedule.id}: Cannot add; it was already added.`
      );
      return false;
    }

    this.schedules.push(schedule);
    this.save();

    this.logger.success(`Schedule ${schedule.id}: added.`);
    return true;
  }

  /**
   * Edit the properties of an existing schedule.
   * @param {string} id The id of the schedule to edit.
   * @param {Object} options The new options to set on the schedule.
   * @returns True if successful.
   */
  editSchedule(id, options) {
    let schedule = this.schedules.find((sch) => sch.id === id);
    if (!schedule) {
      this.logger.error(`Schedule ${id}: Cannot edit; schedule was not found.`);
      return false;
    }

    for (let option in options) {
      if (
        !Object.prototype.hasOwnProperty.call(options, option) ||
        typeof schedule[option] === "undefined"
      )
        continue;

      schedule[option] = options[option];
    }

    this.save();

    this.logger.success(`Schedule ${id}: edited.`);
    return true;
  }

  /**
   * Remove a schedule.
   * @param {string} id The id of the schedule to remove.
   * @returns True if successful.
   */
  removeSchedule(id) {
    let schedule = this.schedules.find((sch) => sch.id === id);
    if (!schedule) {
      this.logger.error(
        `Schedule ${id}: Cannot delete; schedule was not found.`
      );
      return false;
    }

    schedule.destroy();
    this.schedules = this.schedules.filter((sch) => sch.id !== id);

    this.save();

    this.logger.success(`Schedule ${id}: deleted.`);
    return true;
  }

  /**
   * Init function; call after receiving the settings for the first time.
   * @param {string} tableDom DOM query string for the table to list schedules.
   */
  init(tableDom) {
    let schedules = this.settings.settings.schedules;

    // Load in 15-second bulb updating, careful to ensure saved version does not override the settings we actually want.
    this.addSchedule(
      new HueSchedule(
        this,
        Object.assign(
          schedules.find((schedule) => schedule.id === "system-15s") || {},
          {
            id: "system-15s",
            name: "Fetch Bulb States",
            tolerance: 0,
            task: "updateBulbs",
            cron: "*/15 * * * * *",
            options: {},
          }
        )
      )
    );

    // Load in one-minute schedule, careful to ensure saved version does not override the settings we actually want.
    this.addSchedule(
      new HueSchedule(
        this,
        Object.assign(
          schedules.find((schedule) => schedule.id === "system-1m") || {},
          {
            id: "system-1m",
            name: "Fetch NWS Weather Alerts",
            tolerance: -1,
            task: "processOneMinuteTasks",
            cron: "0 * * * * *",
            options: {},
          }
        )
      )
    );

    // Load in five-minute schedule, careful to ensure saved version does not override the settings we actually want.
    this.addSchedule(
      new HueSchedule(
        this,
        Object.assign(
          schedules.find((schedule) => schedule.id === "system-5m") || {},
          {
            id: "system-5m",
            name: "Fetch Tempest Weather; Update Sun/Cloud/Brightness/Lightning Bulbs; 5-minute Weather Alert Sequence",
            tolerance: -1,
            task: "processFiveMinuteTasks",
            cron: "0 */5 * * * *",
            options: {},
          }
        )
      )
    );

    // Load in non-system schedules.
    schedules
      .filter((schedule) => !schedule.id.startsWith("system-"))
      .forEach((schedule) => this.addSchedule(new HueSchedule(this, schedule)));

    // Run schedules which are late
    this.schedules
      .filter((schedule) => schedule.tolerance > 0 || schedule.tolerance === -1)
      .forEach((schedule) => {
        let prevSchedule = later.schedule(schedule.schedule).prev(1);

        // Did we run on the most recent previous schedule? Do not continue.
        if (moment(schedule.lastRun).isSameOrAfter(moment(prevSchedule)))
          return;

        // Did we pass the tolerance minutes? Do not continue.
        if (
          schedule.tolerance > -1 &&
          moment().diff(moment(prevSchedule), "minutes") >= schedule.tolerance
        )
          return;

        // At this point, run the schedule.
        schedule.run();
      });

    // Initialize table
    this.table = $(tableDom).DataTable({
      paging: true,
      data: [],
      columns: [
        { title: "Name" },
        { title: "Schedule" },
        { title: "Last Run" },
        { title: "Actions" },
      ],
      columnDefs: [{ responsivePriority: 1, targets: 3 }],
      order: [[0, "asc"]],
      pageLength: 25,
      drawCallback: () => {
        // Reset action button click events on each draw of the table.
        $(".btn-schedule-edit").off("click");
        $(".btn-schedule-delete").off("click");

        $(".btn-schedule-edit").on("click", (e) => {
          let schedule = this.schedules.find(
            (sch) => sch.id === $(e.currentTarget).data("id")
          );
          if (!schedule) return;

          // SCHEDULE TASK
          switch (schedule.task) {
            case "state":
              this.form_state(schedule.toJSON());
              break;
          }
        });

        $(".btn-schedule-delete").on("click", (e) => {
          let schedule = this.schedules.find(
            (sch) => sch.id === $(e.currentTarget).data("id")
          );
          if (!schedule) return;

          confirmDialog(
            `Are you sure you want to <strong>Permanently Delete</strong> the schedule "${schedule.name}"?`,
            schedule.name,
            () => {
              let destroyed = this.removeSchedule(schedule.id);
              if (destroyed) {
                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Schedule removed",
                  autohide: true,
                  delay: 5000,
                  body: `Schedule has been removed`,
                  position: "bottomRight",
                });
              }
            }
          );
        });
      },
    });

    this.updateTable();
  }

  /**
   * Update the table with current active schedules.
   */
  updateTable() {
    if (!this.table) return;

    this.table.clear();

    this.schedules.forEach((schedule) => {
      this.table.row.add([
        schedule.name,
        schedule.cron ? window.cronstrue.toString(schedule.cron) : "Unknown",
        moment(schedule.lastRun).format("YYYY-MM-DD h:mm:ss A"),
        !schedule.id.startsWith("system-")
          ? `<div class="btn-group">
          <button class="btn btn-sm btn-warning btn-schedule-edit" data-id="${schedule.id}" title="Edit Schedule"><i class="fas fa-edit"></i></button>
      <button class="btn btn-sm btn-danger btn-schedule-delete" data-id="${schedule.id}" title="Delete Schedule"><i class="fas fa-trash"></i></button>
          </div>`
          : `<span class="badge badge-danger">System Task</span>`,
      ]);
    });

    this.table.draw(false);
  }

  /**
   * Re-save schedules to settings.
   */
  save() {
    window.ipc.main.send(`settings`, [
      `schedules`,
      this.schedules.map((schedule) => schedule.toJSON()),
    ]);

    this.updateTable();
  }

  /**
   * Tell the main process to run one minute tasks.
   * @param {Object} options
   */
  task_processOneMinuteTasks(options) {
    window.ipc.main.send("one-minute-tasks", [options]);
  }

  /**
   * Tell the main process to run five minute tasks.
   * @param {Object} options
   */
  task_processFiveMinuteTasks(options) {
    window.ipc.main.send("five-minute-tasks", [options]);
  }

  /**
   * Tell the main process to run an update on bulb states.
   * @param {Object} options
   */
  task_updateBulbs(options) {
    window.ipc.main.send("update-bulbs", [options]);
  }

  // SCHEDULE TASK
  /**
   * Open a form for adding or editing a state task.
   * @param {object} data When editing a task, the original data.
   */
  form_state(data) {
    let form = new HueScheduleAlpaca(this, "state")
      .addBulbs()
      .addStateOptions();
    if (data) form.setData(data);

    this.modal.iziModal("open");
    this.modal.title = "Schedule - Flash Bulbs";
    this.modal.body = "";

    $(this.modal.body).alpaca(form.toJSON());
  }

  /**
   * Add a cycle for changing the bulb state.
   */
  task_state(options) {
    for (
      let i = 0;
      i < (options.state.alert === "select" ? options.cycles : 1);
      i++
    ) {
      this.manager.addCycle(
        new HueCycle({
          bulbs: options.bulbs,
          ignoreIfSaturation: options.ignoreIfSaturation,
          backupTolerance: 15,
          task: "state",
          cycle: {
            power: options.power,
            state: {
              alert: options.state.alert,
              hue: HueScheduleAlpaca.colorStringToObject(options.state.hue),
              sat: HueScheduleAlpaca.colorStringToObject(options.state.sat),
              bri: HueScheduleAlpaca.colorStringToObject(options.state.bri),
              ct: HueScheduleAlpaca.colorStringToObject(options.state.ct),
            },
            persist: options.persist,
            setAsReturnColor: options.persist ? 2 : 0,
          },
        })
      );
    }
  }
}

/**
 * A single schedule / task.
 * @requires createUUID() UUID generator function.
 * @requires later breejs/later
 */
class HueSchedule {
  /**
   * Construct the schedule.
   * @param {HueScheduleManager} scheduleManager The schedule manager which created this schedule.
   * @param {Object} options Options for this schedule.
   * @param {string} options.name Name for the schedule.
   * @param {string} options.lastRun moment timestamp when the schedule last ran.
   * @param {number} options.tolerance The number of minutes late this schedule should be allowed to execute from its next scheduled time. Use -1 for no limit, or 0 for never allow late runs (the default).
   * @param {string} options.cron Cron expression that defined when this schedule should run.
   * @param {Array} options.schedule If cron is not passed, you should pass a later.js schedule array here instead. The schedule will not begin running until one is set.
   * @param {string} options.task Task to execute for this function.
   * @param {Object} options.options Options specific for the schedule task.
   */
  constructor(scheduleManager, options) {
    this.manager = scheduleManager;

    /**
     * Unique id for the schedule. This should only be explicitly set when loading an existing schedule from settings.
     */
    this.id = options.id || createUUID();

    /**
     * Name of the schedule.
     */
    this.name = options.name || "Unknown";

    /**
     * Moment timestamp when this schedule was last run. Should be explicitly set for existing schedules, otherwise defaults to now for new schedules.
     * @type string
     */
    this.lastRun = options.lastRun || moment().valueOf();

    /**
     * The number of minutes late this schedule should be allowed to execute from its next scheduled time. Use -1 for no limit, or 0 for never allow late runs (the default).
     * @type number
     */
    this.tolerance = options.tolerance || 0;

    /**
     * REQUIRED: Name of the task / function to execute from the schedule manager for this schedule.
     */
    this.task = options.task;
    if (typeof this.task !== "string")
      throw new Error("Task is required for a schedule");

    this._cron;
    this.cron = options.cron || "0 0 * * * *";

    this._schedule;
    this.schedule = options.schedule || later.parse.cron(this.cron, true);

    /**
     * Options specific for this schedule task which is passed to the manager when executing the task.
     * @type Object
     */
    this.options = options.options;

    /**
     * The later interval runner which executes this schedule.
     */
    this.interval;
  }

  /**
   * Returns the later.js schedule for this schedule so operations can be performed.
   */
  get laterSchedule() {
    return later.schedule(this.schedule);
  }

  /**
   * The CRON expression defining when this schedule should run, if applicable.
   */
  get cron() {
    return this._cron;
  }

  /**
   * Set a new CRON expression to define when this schedule runs. Immediately is parsed into the schedule property.
   */
  set cron(value) {
    this._cron = value;

    if (typeof value === "string")
      this.schedule = later.parse.cron(value, true);
  }

  /**
   * The later.js schedule object for this schedule.
   * @type Object
   */
  get schedule() {
    return this._schedule;
  }

  /**
   * Set a new later.js schedule, which will reset the interval automatically.
   */
  set schedule(value) {
    this._schedule = value;

    // Reset interval
    if (this.interval) this.interval.clear();
    this.interval = later.setInterval(() => this.run(), this._schedule);
  }

  /**
   * Convert this class instance to an object for storage in the main process.
   * @returns {object}
   */
  toJSON() {
    return {
      id: this.id,
      name: this.name,
      lastRun: this.lastRun,
      tolerance: this.tolerance,
      task: this.task,
      cron: this.cron,
      schedule: this.schedule,
      options: this.options,
    };
  }

  /**
   * Run this schedule.
   */
  run() {
    // Update lastRun time, and re-save all schedules in the manager so the new lastRun is updated.
    this.lastRun = moment().valueOf();
    this.manager.save();

    console.info(`Schedule ${this.id}: Executed with task ${this.task}.`);

    // SCHEDULE TASK
    switch (this.task) {
      case "processOneMinuteTasks":
        this.manager.task_processOneMinuteTasks(this.options);
        break;
      case "processFiveMinuteTasks":
        this.manager.task_processFiveMinuteTasks(this.options);
        this.manager.logger.info(
          `Schedule ${this.id}, task ${this.task}: Started.`
        );
        break;
      case "updateBulbs":
        this.manager.task_updateBulbs(this.options);
        break;
      case "state":
        this.manager.task_state(this.options);
        break;
      default:
        this.manager.logger.error(
          `Schedule ${this.id}: Task name ${this.task} is invalid. Schedule was skipped.`
        );
        console.error(
          `Schedule ${this.id}: Task name ${this.task} is invalid. Schedule was skipped.`
        );
    }
  }

  /**
   * Call this before removing or editing the schedule to deactivate the timer.
   */
  destroy() {
    if (this.interval) this.interval.clear();
  }
}

/**
 * Construct an Alpaca schema for adding or editing schedules.
 * @requires $ jQuery
 * @requires $.fn.alpaca Alpacajs
 */
class HueScheduleAlpaca {
  /**
   * Construct the class
   * @param {HueScheduleManager} manager The schedule manager which called this class.
   * @param {string} task The name of the task for the schedule.
   * @returns this
   */
  constructor(manager, task) {
    this.manager = manager;
    this.task = task;

    this.options = {
      schema: {
        type: "object",
        properties: {
          id: {
            type: "string",
            readonly: true,
            title: "id",
          },
          task: {
            type: "string",
            readonly: true,
            default: this.task,
            title: "Task",
          },
          name: {
            type: "string",
            required: true,
            title: "Descriptive Name",
          },
          cron: {
            type: "string",
            required: true,
            title: "CRON Expression",
          },
          tolerance: {
            type: "number",
            minimum: -1,
            required: true,
            default: 15,
            title: "Schedule Tolerance (minutes)",
          },
          options: {
            type: "object",
            properties: {},
          },
        },
      },
      options: {
        fields: {
          id: {
            helper: "This is used in the debug logs (once created).",
          },
          name: {
            helper:
              "Provide a name to describe this schedule on the schedules page.",
          },
          cron: {
            helpers: [
              "This defines when the schedule will run. You must include the seconds identifier and, optionally, include a 7th year identifier.",
              `You can use this handy <a href="https://www.programmertools.online/generator/cron_expression.html" target="_blank">Programmer Tools</a> to help you create a CRON expression.`,
              `After typing an expression and clicking outside the text box, a message will appear below showing you the human-understandable explanation of the expression you typed. That way, you can confirm whether or not you typed the correct expression.`,
              `<strong>Caution!</strong>: While you must specify seconds in your cron, it is not recommended making a schedule that runs multiple times within a minute if it controls light bulbs. This could back up the bulb cycle queue.`,
            ],
            validator: function (callback) {
              let value = this.getValue();
              let splitter = value.split(" ");
              if (splitter.length < 6 || splitter.length > 7) {
                callback({
                  status: false,
                  message: `Error: CRON expression requires at least 6 parts but no more than 7 parts: [seconds] [minutes] [hours] [days of month] [months] [days of week] [optional years]`,
                });
                return;
              }

              let human = window.cronstrue.toString(value);
              if (human.startsWith("Error:")) {
                callback({
                  status: false,
                  message: human,
                });
                return;
              }
              callback({
                status: true,
                message: `This will run ${human}`,
              });
            },
          },
          tolerance: {
            helper:
              "If this schedule misses running at a scheduled time (eg. the app was closed), allow running this schedule if it is no more than this many minutes late. Use -1 for infinity, or 0 to disallow running missed schedules late.",
          },
          options: {
            fields: {},
          },
        },
        form: {
          buttons: {
            submit: {
              title: "Save Schedule",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                let value = form.getValue();

                const makeNewSchedule = () => {
                  let schedule = new HueSchedule(this.manager, {
                    task: this.task,
                    name: value.name,
                    tolerance: value.tolerance,
                    cron: value.cron,
                    options: value.options,
                  });

                  this.manager.addSchedule(schedule);
                };

                if (value.id) {
                  let schedule = this.manager.schedules.find(
                    (schedule) => schedule.id === value.id
                  );
                  if (schedule) {
                    this.manager.editSchedule(value.id, {
                      name: value.name,
                      tolerance: value.tolerance,
                      cron: value.cron,
                      options: value.options,
                    });
                  } else {
                    makeNewSchedule();
                  }
                } else {
                  makeNewSchedule();
                }

                this.manager.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Schedule saved",
                  autohide: true,
                  delay: 5000,
                  body: `Schedule has been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      view: {
        wizard: {
          hideSubmitButton: true,
          bindings: {
            id: 1,
            task: 1,
            name: 1,
            cron: 2,
            tolerance: 2,
            options: 3,
          },
          steps: [
            {
              title: "Basic Info",
            },
            {
              title: "Schedule",
            },
            {
              title: "Options",
            },
          ],
        },
      },
      data: {},
    };

    return this;
  }

  /**
   * Parse an input color string from Alpaca to either a number or object for use with the HueCycle.
   * @param {string} value The input
   * @returns {number|object|undefined} A number for an absolute value, an object for a relative value.
   */
  static colorStringToObject(value) {
    if (typeof value === "undefined" || value === null || value === "")
      return undefined; // No value? Return undefined.

    let temp = undefined; // The value we return
    let returnValue = {}; // Object construction

    // Determine what to do depending on the first character of value
    switch (value.charAt(0)) {
      // Mathematical operators
      case "+":
      case "-":
      case "*":
      case "/":
        // Set returnValue object to the mathematical operator as key, the rest (parsed as a float) as value.
        returnValue[value.charAt(0)] = parseFloat(
          value.replace(value.charAt(0), "")
        );
        temp = returnValue;
        break;
      default: // No operators? Use float parsed value.
        temp = parseFloat(value);
    }

    return temp;
  }

  /**
   * Return the Alpaca options.
   * @returns {object} Alpaca options
   */
  toJSON() {
    return this.options;
  }

  /**
   * Add configuration for bulbs to this schedule.
   * @returns this
   */
  addBulbs() {
    this.options.schema.properties.options.properties = Object.assign(
      this.options.schema.properties.options.properties,
      {
        bulbs: {
          type: "array",
          title: "Applicable Bulbs",
          items: {
            type: "string",
            enum: Object.keys(this.manager.manager.bulbs),
          },
        },
        ignoreIfSaturation: {
          type: "boolean",
          title: "Ignore bulbs with saturation (color)",
        },
      }
    );
    this.options.options.fields.options.fields = Object.assign(
      this.options.options.fields.options.fields,
      {
        bulbs: {
          type: "checkbox",
          optionLabels: Object.values(this.manager.manager.bulbs).map(
            (bulb) => `${bulb.name} (${bulb.uniqueid})`
          ),
        },
        ignoreIfSaturation: {
          type: "checkbox",
          rightLabel: "Yes",
          helper:
            "Check this box to IGNORE bulbs that are set on a color / saturation is not 0 / not set on a white light (at the time the schedule runs)",
        },
      }
    );
    return this;
  }

  /**
   * Add options for controlling bulb power for power and color tasks. Use addTurnOnFirst() for waveform tasks.
   * @param {boolean} required Specify true when using the power task, false for all other tasks.
   * @returns this
   */
  addPowerOptions(required) {
    this.options.schema.properties.options.properties = Object.assign(
      this.options.schema.properties.options.properties,
      {
        power: {
          type: "number",
          title: "Bulb Power",
          required: required,
          enum: [0, 1, 2],
        },
      }
    );
    this.options.options.fields.options.fields = Object.assign(
      this.options.options.fields.options.fields,
      {
        power: {
          type: "select",
          noneLabel: "Leave At Current Power (flash ignores off bulbs)",
          optionLabels: [
            "Turn Off (flash ignores off bulbs)",
            "Turn On",
            "Toggle On / Off",
          ],
        },
      }
    );

    return this;
  }

  /**
   * Add options for specifying bulb color / state.
   * @returns this
   */
  addStateOptions() {
    this.options.schema.properties.options.properties = Object.assign(
      this.options.schema.properties.options.properties,
      {
        power: {
          type: "number",
          enum: [0, 1, 2],
          title: "Power",
        },
        repeat: {
          title: "Repeat",
          type: "number",
          minimum: 1
        },
        state: {
          type: "object",
          title: "Color / State",
          properties: {
            hue: {
              type: "string",
              title: "Hue / Color (degrees on color wheel)",
            },
            sat: {
              type: "string",
              title: "Saturation / Color Intensity (0 - 100)",
            },
            bri: {
              type: "string",
              title: "Brightness (0 - 100)",
            },
            ct: {
              type: "string",
              title: "Kelvin / Warmth of White Light (2000 - 6500 degrees)",
            },
            alert: {
              type: "string",
              required: true,
              enum: ["none", "select", "lselect"],
              title: "State",
            },
          },
        },
        persist: {
          type: "boolean",
          title: "Persist Color?",
        },
      }
    );

    // Define a color validation function
    const colorValidator = function (callback) {
      let value = this.getValue();
      let parsedValue = HueScheduleAlpaca.colorStringToObject(value);
      if (
        typeof parsedValue !== "object" &&
        typeof parsedValue !== "undefined" &&
        isNaN(parsedValue)
      ) {
        callback({
          status: false,
          message: `Must be a number or contain a mathematical operator followed by a number (eg. +15, -30, *1.5, /2)`,
        });
        return;
      }

      callback({
        status: true,
      });
    };

    this.options.options.fields.options.fields = Object.assign(
      this.options.options.fields.options.fields,
      {
        power: {
          type: "select",
          noneLabel: "Leave At Current Power",
          optionLabels: ["Turn Off", "Turn On", "Toggle On / Off"],
        },
        state: {
          helpers: [
            "Blank = use bulb's current value",
            "Number = Change to the specified value",
            "+-*/number = Add, subtract, multiply, or divide bulb's current value with the specified value (eg. +30, -15, *1.5, /2).",
            "Hue is a wheel, so > 360 starts over at 0, and < 0 goes back to 360. Saturation and brightness limit at minimum 0, maximum 100. Kelvin limits at minimum 2000, maximum 6500.",
          ],
          fields: {
            hue: {
              validator: colorValidator,
            },
            sat: {
              validator: colorValidator,
              helper: "0 = white (uses kelvin)",
            },
            bri: {
              validator: colorValidator,
            },
            ct: {
              validator: colorValidator,
            },
            alert: {
              type: "select",
              optionLabels: [
                "Turn to specified color",
                "Pulse color (repeat) number of times",
                "Flash color for (repeat) cycles of 15 seconds each",
              ],
            },
          },
        },
        repeat: {
          helper:
            "Number of times this state change should be executed (e.g. number of flashes / flash cycles)",
        },
        persist: {
          type: "checkbox",
          rightLabel: "Yes",
          helpers: [
            "Only when flashing / pulsing bulbs; ignored if not flashing / pulsing bulbs.",
            "If checked, then the bulbs will remain the specified color after flashing.",
            "If unchecked, the bulbs will return to their previous state after the flash is complete.",
          ],
        },
      }
    );

    return this;
  }

  /**
   * Set initial data on this form.
   * @param {Object} data Initial data (Warning! Modified in place)
   * @returns this
   */
  setData(data) {
    // Never allow changing the task property.
    data.task = this.task;

    this.options.data = data;
    return this;
  }
}
