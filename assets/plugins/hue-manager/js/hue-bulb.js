/**
 * A class for one Hue bulb.
 * @requires HueCycle
 */
class HueBulb {
  /**
   * Constructor to manage a new single Hue bulb.
   *
   * @param {HueManager} manager HueManager class
   * @param {object} bulb Bulb object / data passed from main
   */
  constructor(manager, bulb) {
    /**
     * @type HueManager
     */
    this.manager = manager;

    /**
     * The bulb's id in the Bridge.
     * @type string
     */
    this._id = bulb.id;

    /**
     * The bulb's MAC address.
     * @type string
     */
    this.uniqueid = bulb.uniqueid;

    /**
     * Queued backup cycles that should be executed (if not too old) when the bulb comes back online.
     * @type HueCycle[]
     */
    this.backupCycles = [];

    /**
     * Object of Functions executed when all cycles are complete (or no cycles queued), or when the bulb comes back online.
     * Generally, this should only set the bulb to a specific color quickly / immediately instead of perform a cycle / effect, unless it is perfectly acceptable for the cycle / effect to be overridden by another.
     * @type Object.<string, function>
     */
    this.returnState = {};

    /**
     * Object of a Hue state (brightness of 0 = turn the bulb off) the bulb should return to after some overriding state ends (such as lightning). This gets set via "setAsReturnColor" (1 = if not already set, 2 = override existing setting) in a bulb cycle.
     * @type Object|null
     */
    this.returnColor = null;

    /**
     * Whether or not this bulb is online.
     * You should use this.online instead.
     * @type {-1 | 0 | 1} (-1 = not registered in the Bridge; 0 = offline; 1 = online)
     */
    this._online = typeof bulb.online !== "undefined" ? bulb.online : -1;

    this._state;
    this._name = bulb.name || bulb.uniqueid;

    this.constructCard();
  }

  constructCard() {
    // Make HTML block
    $("#hue-bulbs").append(`<div class="col-12 col-md-6 col-lg-4 hue-bulb-${
      this.id
    }">
                      <div
                        class="small-box ${
                          this._online !== 1
                            ? this._online === 0
                              ? `bg-warning`
                              : `bg-danger`
                            : this._state && this._state.on
                            ? `bg-success`
                            : `bg-secondary`
                        } hue-bulb-${this.id}-box"
                      >
                        <div class="inner">
                          <h3 style="font-size: 1.1em;">${this.uniqueid}</h3>
  
                          <p class="hue-bulb-${this.id}-label">${this.name}</p>
                        </div>
                        <div class="icon">
                          <i class="fas fa-lightbulb" id="hue-bulb-${
                            this.id
                          }-icon" style="color: ${
                            this._state
                              ? this.manager.colorToHex(this._state, true)
                              : `#000000`
                          }; opacity: 0.9;"></i>
                        </div>
                        <div class="small-box-footer" style="cursor: pointer;" id="hue-bulb-${
                          this.id
                        }-identify">
                          Flash / Identify
                        </div>
                        <div class="small-box-footer" style="cursor: pointer;" id="hue-bulb-${
                          this.id
                        }-toggle">
                          Toggle On / Off
                        </div>
                        <div class="small-box-footer" style="cursor: pointer;" id="hue-bulb-${
                          this.id
                        }-color">
                          Change Color
                        </div>
                      </div>
                    </div>`);

    // Add click events
    window.requestAnimationFrame(() => {
      const changeColor = (value) => {
        this.manager.logger.info(
          `Bulb ${this.id}: Setting returnState to change color to ${value} once bulb has no running cycles.`
        );
        this.returnState["changeColor"] = () => {
          this.manager.addCycle(
            new HueCycle({
              bulbs: [this.uniqueid],
              task: "state",
              backupTolerance: 0,
              cycle: {
                persist: true,
                state: this.manager.hexToColor(value),
                setAsReturnColor: 2,
              },
            }),
            true
          );
        };
      };
      $(`#hue-bulb-${this.id}-identify`).on("click", () => {
        this.manager.logger.info(`Bulb ${this.uniqueid}: Identifying.`);
        this.manager.addCycle(
          new HueCycle({
            bulbs: [this.uniqueid],
            task: "state",
            backupTolerance: 0,
            cycle: {
              persist: false,
              state: {
                // Red
                hue: 0,
                sat: 100,
                bri: 100,
                alert: "lselect",
              },
            },
          })
        );
      });
      $(`#hue-bulb-${this.id}-toggle`).on("click", () => {
        this.manager.logger.info(`Bulb ${this.uniqueid}: Toggling power.`);
        this.manager.addCycle(
          new HueCycle({
            bulbs: [this.uniqueid],
            task: "state",
            backupTolerance: 5,
            cycle: {
              power: 2,
              setAsReturnColor: 2,
            },
          })
        );
      });
      $(`#hue-bulb-${this.id}-color`).on("click", () => {
        $("#hue-bulb-color").modal("show");
        $("#hue-bulb-color .modal-body").html("");
        $("#hue-bulb-color .modal-body").alpaca({
          schema: {
            title: `Change color of ${this._name} (${this.uniqueid})`,
            type: "object",
            properties: {
              color: {
                type: "string",
                required: true,
                default: this.manager.colorToHex(this._state, true),
              },
            },
          },
          options: {
            fields: {
              color: {
                type: "color",
                helper:
                  "Click in an empty area on this window after picking your color to set it on the bulb.",
                events: {
                  change: function () {
                    let value = this.getValue();
                    changeColor(value);
                  },
                },
              },
            },
          },
        });
      });
    });
  }

  get id() {
    return this._id;
  }

  set id(value) {
    if (value !== this._id) {
      $(`.hue-bulb-${this.id}`).remove();
      this._id = value;
      this.constructCard();
    }
  }

  /**
   * Change the bulb name
   *
   * @param {string} value New label
   */
  set name(value) {
    this._name = value;
    $(`.hue-bulb-${this.id}-label`).html(value);
  }

  get name() {
    return this._name;
  }

  /**
   * Alias for state.on. Change the power of the bulb.
   */
  set status(value) {
    this.state = Object.assign(this._state, { on: value });
  }

  get status() {
    return this._state.on ? "on" : "off";
  }

  /**
   * Set whether or not this bulb is currently connected to the network.
   *
   * @param {-1 | 0 | 1} value Is the bulb connected to the network? -1 = not registered, 0 = offline, 1 = online.
   */
  set online(value) {
    this._online = value;
    if (value === -1) {
      $(`.hue-bulb-${this.id}-box`).removeClass([
        "bg-success",
        "bg-warning",
        "bg-secondary",
      ]);
      $(`.hue-bulb-${this.id}-box`).addClass("bg-danger");
    } else if (value === 0) {
      $(`.hue-bulb-${this.id}-box`).removeClass([
        "bg-success",
        "bg-danger",
        "bg-secondary",
      ]);
      $(`.hue-bulb-${this.id}-box`).addClass("bg-warning");
    } else {
      this.processBackupCycles();
      $(`.hue-bulb-${this.id}-box`).removeClass([
        "bg-danger",
        "bg-warning",
        "bg-secondary",
        "bg-success",
      ]);
      if (this._state && this._state.on) {
        $(`.hue-bulb-${this.id}-box`).addClass("bg-success");
      } else {
        $(`.hue-bulb-${this.id}-box`).addClass("bg-secondary");
      }
    }
  }

  get online() {
    return this._online;
  }

  /**
   * Set the bulb power and color.
   *
   * @param {boolean} value True if the bulb is on, false if not.
   */
  set state(value) {
    this._state = value;
    if (this._online === 1) {
      $(`.hue-bulb-${this.id}-box`).removeClass([
        "bg-danger",
        "bg-warning",
        "bg-secondary",
        "bg-success",
      ]);
      if (value.on) {
        $(`.hue-bulb-${this.id}-box`).addClass("bg-success");
      } else {
        $(`.hue-bulb-${this.id}-box`).addClass("bg-secondary");
      }
    }
    $(`#hue-bulb-${this.id}-icon`).css(
      "color",
      this.manager.colorToHex(value, true)
    );
  }

  get state() {
    return this._state;
  }

  /**
   * Send all queued backup cycles over to the manager
   */
  processBackupCycles() {
    if (this.backupCycles.length > 0) {
      console.log(
        `Hue bulb ${this.id}: Processing ${this.backupCycles.length} backup cycles`
      );
      this.manager.logger.success(
        `Bulb ${this.id}: Now online. Processing ${this.backupCycles.length} backup cycles.`
      );

      this.backupCycles
        // Only add cycles if it does not exceed backupTolerance
        .filter((cycle) =>
          moment(cycle.timestamp)
            .add(
              typeof cycle.backupTolerance !== "undefined"
                ? cycle.backupTolerance
                : 5,
              "minutes"
            )
            .isAfter(moment())
        )
        .forEach((cycle) => this.manager.addCycle(cycle, true));

      // Empty the array of backupCycles.
      this.backupCycles = [];
    }
  }
}
