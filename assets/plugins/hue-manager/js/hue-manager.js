/**
 * Class to manage all Hue bulbs.
 * @requires hue-utils.js
 */
class HueManager {
  /**
   * Construct the class
   * @param {Logger} logger Logger instance
   */
  constructor(logger) {
    /**
     * Logger.
     * @type Logger
     */
    this.logger = logger;
    /**
     * Cached list of LIFXbulbs. Key = bulb ID, value = HueBulb instance.
     * @type Object.<string, HueBulb>
     */
    this.bulbs = {};

    /**
     * Queued bulb cycles / operations.
     * @type HueCycle[]
     */
    this.cycles = [];

    /**
     * Set this to true after the Hue Bridge first reports discovered bulbs. Cycles will not run (even when forced) until this is true.
     * @type boolean
     */
    this.cyclesReadyToRun = false;

    /**
     * Interval that runs to check which cycles we can execute.
     */
    this.cycleInterval = setInterval(() => {
      if (!this.cyclesReadyToRun) return;

      let activeBulbsLow = [];
      let activeBulbsHigh = [];

      (() => {
        if (this.cycles.length === 0) return; // Skip if we have no cycles in the queue

        let cycles = this.cycles.filter((cycle) => !cycle.running); // Get all non-running cycles.
        if (cycles.length === 0) return; // Skip if there are no non-running queued cycles.

        // Determine which bulbs are currently active in a cycle
        let temp = this.cycles.filter(
          (cycle) => cycle.running && cycle.bulbs && cycle.bulbs.length > 0
        );
        temp.map((cycle) => {
          if (cycle.cycle.highPriority) {
            cycle.bulbs.map((bulb) => activeBulbsHigh.push(bulb));
          } else {
            cycle.bulbs.map((bulb) => activeBulbsLow.push(bulb));
          }
        });

        // Loop through each cycle until/unless we get one that can be run
        for (let cycle in cycles) {
          if (Object.prototype.hasOwnProperty.call(cycles, cycle)) {
            if (!cycles[cycle].bulbs || cycles[cycle].bulbs.length === 0)
              continue;

            // Cycles can be run if it is high priority and all bulbs are not running a high priority cycle, or if none of the bulbs are currently running a cycle.
            let canRun = true;
            cycles[cycle].bulbs.map((bulb) => {
              if (activeBulbsHigh.indexOf(bulb) !== -1) canRun = false;
              /* TODO: highPriority overrides not yet working correctly in main
              if (
                !cycles[cycle].cycle.highPriority &&
                activeBulbsLow.indexOf(bulb) !== -1
              )
                canRun = false;
                */
              if (activeBulbsLow.indexOf(bulb) !== -1) canRun = false;
            });

            if (canRun) {
              cycles[cycle].run(this);
              if (cycles[cycle].highPriority) {
                activeBulbsHigh.concat(cycles[cycle].bulbs);
              } else {
                activeBulbsLow.concat(cycles[cycle].bulbs);
              }
              break;
            }
          }
        }
      })();

      // Loop through each bulb not currently running a cycle and execute returnState if the bulb is online.
      for (let bulb in this.bulbs) {
        if (!Object.prototype.hasOwnProperty.call(this.bulbs, bulb)) continue;
        if (activeBulbsLow.indexOf(this.bulbs[bulb].uniqueid) !== -1) continue;
        if (activeBulbsHigh.indexOf(this.bulbs[bulb].uniqueid) !== -1) continue;

        for (let key in this.bulbs[bulb].returnState) {
          if (
            !Object.prototype.hasOwnProperty.call(
              this.bulbs[bulb].returnState,
              key
            )
          )
            continue;
          if (this.bulbs[bulb].online !== 1) {
            delete this.bulbs[bulb].returnState[key];
            this.logger.warn(
              `Bulb ${this.bulbs[bulb].id}: returnState ${key} SKIPPED; bulb is offline.`
            );
            continue;
          }
          this.bulbs[bulb].returnState[key]();
          delete this.bulbs[bulb].returnState[key];
        }
      }
    }, 500);

    /**
     * Every minute, clean up expired backup cycles from bulbs.
     */
    this.cleanupBackupCycles = setInterval(() => {
      for (let bulb in this.bulbs) {
        if (!Object.prototype.hasOwnProperty.call(this.bulbs, bulb)) continue;

        this.bulbs[bulb].backupCycles = this.bulbs[bulb].backupCycles.filter(
          (cycle) =>
            moment(cycle.timestamp)
              .add(
                typeof cycle.backupTolerance !== "undefined"
                  ? cycle.backupTolerance
                  : 5,
                "minutes"
              )
              .isAfter(moment())
        );
      }
    }, 60000);
  }

  /**
   * Add a new Hue bulb to the manager
   *
   * @param {object} bulb Initial bulb data. Should contain at least id.
   */
  addBulb(bulb) {
    if (typeof this.bulbs[bulb.uniqueid] === "undefined") {
      this.bulbs[bulb.uniqueid] = new HueBulb(this, bulb);
    } else {
      this.processBulb(bulb);
    }
  }

  /**
   * Process multiple bulbs received from the main process, and mark other bulbs not defined as offline.
   *
   * @param {Array} bulbs Array of bulbs currently on the network
   */
  processBulbs(bulbs) {
    let checkedBulbs = [];

    // Process bulbs sent
    bulbs.forEach((bulb) => {
      checkedBulbs.push(bulb.uniqueid);
      this.addBulb(bulb);
    });

    // Set all bulbs not in the returned data to -1;
    for (var bulb in this.bulbs) {
      if (Object.prototype.hasOwnProperty.call(this.bulbs, bulb)) {
        if (checkedBulbs.indexOf(bulb.uniqueid) === -1) {
          this.bulbs[bulb.uniqueid].online = -1;
        }
      }
    }
  }

  /**
   * Update a bulb's information.
   *
   * @param {object} state State data received from main process.
   */
  processBulb(bulb) {
    if (this.bulbs[bulb.uniqueid]) {
      for (let key in bulb) {
        // Only allow editing of certain properties
        if (
          !Object.prototype.hasOwnProperty.call(bulb, key) ||
          ["name", "status", "online", "state", "id"].indexOf(key) === -1
        )
          continue;

        this.bulbs[bulb.uniqueid][key] = bulb[key];
      }
    } else {
      this.addBulb(bulb);
    }
  }

  /**
   * Add a cycle to the queue.
   *
   * @param {HueCycle} cycle The cycle to add.
   * @param {boolean} startAsap If true, the cycle will be loaded into the beginning of the cycles array so it starts as soon as possible.
   */
  addCycle(cycle, startAsap = false) {
    console.log(`Cycle ${cycle.id} added`);
    cycle.running = false; // Sometimes running will be set to true if adding from a backup cycle.

    if (!startAsap) {
      this.cycles.push(cycle);
    } else {
      this.cycles.unshift(cycle);
    }
  }

  /**
   * Add a cycle to the queue to return a bulb to its returnColor (and clear the returnColor).
   * 
   * @param {Array<HueBulb>} bulbs Array of Hue bulbs to returnColor
   */
  addReturnColorCycle(bulbs) {
    bulbs.forEach((bulb) => {
      if (bulb.returnColor === null) {
        return;
      }

      appManager.addCycle(
        new HueCycle({
          bulbs: [bulb.uniqueid],
          task: "state",
          backupTolerance: 15,
          ignoreIfSaturation: false,
          cycle: {
            persist: true,
            setAsReturnColor: 0,
            state: Object.assign({}, bulb.returnColor), // Dereference because we will be setting it to null
          },
        })
      );

      this.bulbs[bulb.uniqueid].returnColor = null;
    });
  }

  /**
   * Remove a completed cycle.
   */
  cycleComplete(id) {
    this.cycles = this.cycles.filter((cycle) => cycle.id != id);
  }

  /**
   * Process requests by the main process to add cycles to a bulb's backup cycles.
   *
   * @param {HueCycle[]} backupCycles Backup cycles that should be added.
   */
  processBackupCycleData(backupCycles) {
    if (backupCycles.length === 0) return;
    backupCycles.map((backupCycle) => {
      backupCycle.bulbs.map((bulb) => {
        if (typeof this.bulbs[bulb] !== "undefined") {
          this.bulbs[bulb].backupCycles.push(backupCycle);
        }
      });
      this.cycleComplete(backupCycle.id);
    });
  }

  /**
   * Process a request to register a bulb's returnColor.
   *
   * @param {Object} data The data object sent from the main process
   */
  processReturnColor(data) {
    if (typeof this.bulbs[data.bulb] === "undefined") {
      return;
    }

    // Do not override existing return colors unless we explicitly want to
    if (this.bulbs[data.bulb].returnColor !== null) {
      if (data.overrideExisting === false) {
        return;
      }
    }

    this.bulbs[data.bulb].returnColor = data.state;
  }

  /**
   * Utility function to convert a hex string color to Hue compatible HSL object.
   *
   * @param {string} hex Hex color
   * @param {boolean} toHueValues Whether to return as hue values instead of standard color values
   * @returns {object} hue, saturation, brightness (standard color values unless toHueValues is true).
   */
  hexToColor(hex, toHueValues = false) {
    let result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex);

    let r = parseInt(result[1], 16);
    let g = parseInt(result[2], 16);
    let b = parseInt(result[3], 16);

    (r /= 255), (g /= 255), (b /= 255);

    let max = Math.max(r, g, b),
      min = Math.min(r, g, b);
    let h,
      s,
      l = (max + min) / 2;

    if (max == min) {
      h = s = 0; // achromatic
    } else {
      var d = max - min;
      s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
      switch (max) {
        case r:
          h = (g - b) / d + (g < b ? 6 : 0);
          break;
        case g:
          h = (b - r) / d + 2;
          break;
        case b:
          h = (r - g) / d + 4;
          break;
      }
      h /= 6;
    }

    // Convert HSL to HSV
    let hsv1 = s * (l < 0.5 ? l : 1 - l);
    s = hsv1 === 0 ? 0 : (2 * hsv1) / (l + hsv1);
    let v = l + hsv1;

    if (toHueValues) {
      return {
        hue: this.colorToHueValue("hue", h * 360),
        sat: this.colorToHueValue("sat", s * 100),
        bri: this.colorToHueValue("bri", v * 100),
      };
    }

    return {
      hue: h * 360,
      sat: s * 100,
      bri: v * 100,
    };
  }

  /**
   * Convert standard color to hex string.
   *
   * @param {object} color standard color
   * @param {boolean} isHueValues Whether the specified color is hue values instead of standard colors
   * @returns {string} hex color string
   */
  colorToHex(color, isHueValues = false) {
    let objcolor;

    if (isHueValues) {
      objcolor = {
        hue: this.hueValueToColor("hue", color.hue),
        sat: this.hueValueToColor("sat", color.sat) / 100,
        bri: this.hueValueToColor("bri", color.bri) / 100,
      };
    } else {
      objcolor = {
        hue: color.hue,
        sat: color.sat / 100,
        bri: color.bri / 100,
      };
    }

    // We need to convert to HSL.
    let l = (2 - objcolor.sat) * objcolor.bri;
    let [s, v] = [
      l === 0 || l === 2
        ? 0
        : (objcolor.sat * objcolor.bri) / (l <= 1 ? l : 2 - l),
      (l * 5) / 10,
    ];

    const a = s * Math.min(v, 1 - v);
    const f = (n) => {
      const k = (n + objcolor.hue / 30) % 12;
      const color2 = v - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
      return Math.round(255 * color2)
        .toString(16)
        .padStart(2, "0"); // convert to Hex and prefix "0" if needed
    };
    return `#${f(0)}${f(8)}${f(4)}`;
  }

  /**
   * Convert a standard color value to its Hue API value.
   *
   * @param @param {"hue" | "sat" | "bri" | "ct"} key
   * @param {number} value
   * @returns {number} The Hue API value
   */
  colorToHueValue(key, value) {
    // Make sure we are within limits
    switch (key) {
      case "hue":
        while (value < 0) {
          value += 360;
        }
        while (value >= 360) {
          value -= 360;
        }
        break;
      case "sat":
      case "bri":
        if (value < 0) value = 0;
        if (value > 100) value = 100;
        break;
      case "ct":
        if (value < 2000) value = 2000;
        if (value > 6500) value = 6500;
        break;
    }

    // Make the conversion
    switch (key) {
      case "hue":
        value = (value / 360) * 65535;
        break;
      case "sat":
        value = (value / 100) * 254;
        break;
      case "bri":
        value = (value / 100) * 254;
        if (value < 1) value = 1;
        break;
      case "ct":
        value = (1 - (value - 2000) / (6500 - 2000)) * (500 - 153) + 153;
        break;
    }

    return value;
  }

  /**
   * Convert a Hue API value to a standard color value
   *
   * @param @param {"hue" | "sat" | "bri" | "ct"} key
   * @param {number} value
   * @returns {number} The standard color value
   */
  hueValueToColor(key, value) {
    // Make sure we are within limits
    switch (key) {
      case "hue":
        while (value < 0) {
          value += 65535;
        }
        while (value >= 65535) {
          value -= 65535;
        }
        break;
      case "sat":
        if (value < 0) value = 0;
        if (value > 254) value = 254;
        break;
      case "bri":
        if (value < 1) value = 1;
        if (value > 254) value = 254;
        break;
      case "ct":
        if (value < 153) value = 153;
        if (value > 500) value = 500;
        break;
    }

    // Make the conversion
    switch (key) {
      case "hue":
        value = (value / 65535) * 360;
        break;
      case "sat":
      case "bri":
        value = (value / 254) * 100;
        break;
      case "ct":
        value = (1 - (value - 153) / (500 - 153)) * (6500 - 2000) + 2000;
        break;
    }

    return value;
  }
}

/**
 * Defines a cycle to run on Hue bulbs, managed by the HueManager.
 * @requires createUUID()
 * @requires moment()
 */
class HueCycle {
  /**
   * Create the Hue bulb cycle.
   *
   * @param {object} options Options for the cycle
   * @param {string[]} options.bulbs Array of bulb uniqueids on which to run this cycle.
   * @param {number} options.backupTolerance Cycle should still run if an offline bulb returns online within this many minutes. Defaults to 5.
   * @param {number} options.stage If backup cycle, which stage of the task at which the cycle should start.
   * @param {LightState} options.prevState If backup cycle, the previous known state of the bulb.
   * @param {"state"} options.task The task to run.
   * @param {boolean} options.ignoreIfSaturation If true, bulbs whose saturation is >= 5 at the time the cycle runs will be ignored.
   * @param {boolean} options.highPriority If true, this cycle will override non-high priority cycles.
   * @param {object} options.cycle Options for the cycle / task.
   * @param {0 | 1 | 2} options.cycle.power Specify power toggling on the bulb (overwrites options.cycle.state.on)
   * @param {object} options.cycle.state The Hue LightState to use
   * @param {boolean} options.cycle.persist (only if options.cycle.state.alert is defined and not "none") keep the bulb at the set color after alert is finished.
   * @param {0 | 1 | 2} options.cycle.setAsReturnColor Whether the color should be set as the bulb's returnColor, which is what the bulb gets set to after an overriding mode ends (such as lightning). 1 = yes, if this is not already set; 2 = override existing setting.
   * @param {number} options.cycle.repeat The number of times this state change should occur.
   */
  constructor(options) {
    /**
     * Unique id for the cycle. This should only be explicitly set when replicating an existing cycle (eg. backup cycles).
     */
    this.id = options.id || createUUID();

    /**
     * Moment timestamp for this cycle. This should only be explicitly set when replicating an existing cycle (eg. backup cycles).
     * @type string
     */
    this.timestamp = options.timestamp || moment().valueOf();

    /**
     * (when a backup cycle) this cycle should still run if the bulb reports online within this many minutes after the cycle timestamp.
     */
    this.backupTolerance = options.backupTolerance;

    /**
     * Ignore this cycle on bulbs whose current saturation is not 0 (white).
     */
    this.ignoreIfSaturation = options.ignoreIfSaturation;

    /**
     * Prioritize this cycle over non-high priority cycles.
     * @type boolean
     */
    this.highPriority = options.highPriority;

    /**
     * Whether or not this cycle is currently running.
     * @type boolean
     */
    this.running = false;

    this._bulbs;
    this.bulbs = options.bulbs;

    /**
     * The task to run.
     */
    this.task = options.task;

    /**
     * The stage number to begin the cycle. This should usually only be set when loading as a backup cycle.
     */
    this.stage = options.stage;

    /**
     * The previous known state of the bulb. This should only be set when loading as a backup cycle.
     */
    this.prevState = options.prevState;

    /**
     * Options for the cycle / task.
     */
    this.cycle = options.cycle;
  }

  /**
   * Array of bulb ids on which to run this cycle. Duplicates are removed automatically.
   * @type string[]
   */
  get bulbs() {
    return this._bulbs;
  }

  set bulbs(value) {
    // Only use unique bulb entries; avoids duplicate bulbs.
    let actualBulbs = [];
    if (value && value.constructor === Array && value.length) {
      value.forEach((bulb) => {
        if (actualBulbs.indexOf(bulb) === -1) actualBulbs.push(bulb);
      });
      this._bulbs = actualBulbs;
    }
  }

  /**
   * Convert this class instance to an object.
   * @returns {object}
   */
  toJSON() {
    return {
      id: this.id,
      timestamp: this.timestamp,
      backupTolerance: this.backupTolerance,
      ignoreIfSaturation: this.ignoreIfSaturation,
      highPriority: this.highPriority,
      running: this.running,
      bulbs: this.bulbs,
      task: this.task,
      stage: this.stage,
      prevState: this.prevState,
      cycle: this.cycle,
    };
  }

  /**
   * Tell the main process to run this cycle.
   * @param {HueManager} manager the Manager that called this function.
   * @param {boolean} force Do not bail if cycle is already running.
   */
  run(manager, force = false) {
    if (this.running && !force) return;
    this.running = true;

    console.log(`triggered cycle ${this.id}.`);

    // Before telling the renderer to run, filter out offline bulbs and immediately set them to a backup cycle.
    this.bulbs
      .filter((bulb) => manager.bulbs[bulb] && manager.bulbs[bulb].online !== 1)
      .forEach((bulb) => {
        console.warn(
          `Cycle ${this.id}: bulb ${bulb} SKIPPED; bulb is offline. Adding to backup cycles.`
        );
        manager.logger.warn(
          `Cycle ${this.id}: bulb ${bulb} SKIPPED; bulb is offline. Adding to backup cycles.`
        );
        manager.bulbs[bulb].backupCycles.push(this);
      });
    this.bulbs = this.bulbs.filter(
      (bulb) => manager.bulbs[bulb] && manager.bulbs[bulb].online === 1
    );

    if (!this.bulbs.length) {
      console.log(`Cycle ${this.id}: SKIPPED; No bulbs online.`);
      manager.logger.log(`Cycle ${this.id}: SKIPPED; no bulbs online.`);
      manager.cycleComplete(this.id);
      return;
    }

    window.ipc.main.send(this.task, [this.toJSON()]);
  }
}
