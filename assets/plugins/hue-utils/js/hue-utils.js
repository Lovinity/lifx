"use strict";

/**
 * Create a UUID
 * @returns {string} A v4 UUID
 */
function createUUID() {
  // Use crypto where possible
  if (typeof crypto !== "undefined" && typeof crypto.randomUUID === "function")
    return crypto.randomUUID();
  if (
    typeof window !== "undefined" &&
    typeof window.crypto !== "undefined" &&
    typeof window.crypto.randomUUID === "function"
  )
    return window.crypto.randomUUID();

  // Fallback
  let dt = new Date().getTime();
  let uuid = "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
    /[xy]/g,
    function (c) {
      let r = (dt + Math.random() * 16) % 16 | 0;
      dt = Math.floor(dt / 16);
      return (c == "x" ? r : (r & 0x3) | 0x8).toString(16);
    }
  );
  return uuid;
}

/**
 * Call a function when an element exists on the document.
 *
 * @param {string} theelement DOM query string of the element to wait for until it exists
 * @param {function(any)} cb Function to call when the element exists
 * @param {number} termination Terminate checking if element not found within this many request animation frames.
 */
function waitForElement(theelement, cb, termination = 250) {
  termination--;
  if (!document.querySelector(theelement)) {
    if (termination > 0) {
      window.requestAnimationFrame(() =>
        waitForElement(theelement, cb, termination)
      );
    } else {
      throw new Error(`waitForElement for element ${theelement} timed out.`);
    }
  } else {
    // eslint-disable-next-line callback-return
    cb(document.querySelector(theelement));
  }
}

/**
 * Create a confirmation dialog with Alpaca forms and iziModal
 *
 * @param {string} description Further information to provide in the confirmation
 * @param {?string} confirmText Require user to type this text to confirm their action (null: use simple yes/no confirmation)
 * @param {function} cb Callback executed when and only if action is confirmed.
 */
function confirmDialog(description, confirmText, cb) {
  let tempModal = new Modal(`Confirm Action`, `bg-warning`, ``, false, {
    overlayClose: false,
    zindex: 5000,
    timeout: false,
    closeOnEscape: false,
    closeButton: false,
    onClosed: () => {
      tempModal = undefined;
    },
  });

  tempModal.iziModal("open");

  tempModal.body = `<p>${description}</p><div id="modal-${tempModal.id}-form"></div>`;

  waitForElement(`#modal-${tempModal.id}-form`, () => {
    $(`#modal-${tempModal.id}-form`).alpaca({
      schema: {
        title: `Confirm Action`,
        type: "object",
        properties: {
          confirmText: {
            type: "string",
            title: `Confirmation`,
            required: confirmText ? true : false,
          },
        },
      },
      options: {
        fields: {
          confirmText: {
            helper: `Please type <strong>${confirmText}</strong> to confirm your action (case sensitive).`,
            hidden: confirmText ? false : true,
            validator: function (callback) {
              let value = this.getValue();
              if (confirmText && value !== `${confirmText}`) {
                callback({
                  status: false,
                  message: `You must type <strong>${confirmText}</strong> to confirm your action.`,
                });
                return;
              }
              callback({
                status: true,
              });
            },
          },
        },
        form: {
          buttons: {
            submit: {
              title: `Yes`,
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                tempModal.iziModal("close");
                cb();
              },
            },
            dismiss: {
              title: `No`,
              click: (form, e) => {
                $(document).Toasts("create", {
                  class: "bg-warning",
                  title: "Action canceled",
                  autohide: true,
                  delay: 10000,
                  body: `You clicked No.`,
                });
                tempModal.iziModal("close");
              },
            },
          },
        },
      },
    });
  });
}

/**
 * Debug logger for the DOM.
 * @requires $ jQuery
 */
class Logger {
  /**
   * Construct the logger
   * @param {string} div The DOM element query where logs should be placed.
   */
  constructor(div) {
    this.div = div;

    // Remove logs older than 3 hours
    this.sweep = setInterval(() => {
      $(".lifx-log-entry").each((i, e) => {
        if (
          moment()
            .subtract(3, "hours")
            .isAfter(moment($(e).data("time")))
        ) {
          $(e).remove();
        }
      });
    }, 60000);
  }

  /**
   * Log something in the debug box on the LIFX page.
   *
   * @param {string} text Text to log.
   * @param {string} textClass Color class to use for the text (undefined: use default)
   */
  log(text, textClass) {
    $(this.div).append(
      `<p><span data-time="${moment().valueOf()}" class="lifx-log-entry${
        textClass ? ` text-${textClass}` : ``
      }">${moment().format("YYYY-MM-DD HH:mm:ss")}: ${text}</span></p>`
    );
  }

  /**
   * Log an info-level entry.
   *
   * @param {string} text Text to log
   * @param {boolean} showToast Also show this entry as a toast
   */
  info(text, showToast = false) {
    this.log(text, "info");

    if (showToast) {
      $(document).Toasts("create", {
        class: "bg-info",
        title: "Logger",
        autohide: true,
        delay: 15000,
        body: text,
        position: "bottomRight",
      });
    }
  }

  /**
   * Log a warn-level entry.
   *
   * @param {string} text Text to log
   * @param {boolean} showToast Also show this entry as a toast
   */
  warn(text, showToast = false) {
    this.log(text, "warning");

    if (showToast) {
      $(document).Toasts("create", {
        class: "bg-warning",
        title: "Logger",
        autohide: true,
        delay: 15000,
        body: text,
        position: "bottomRight",
      });
    }
  }

  /**
   * Log an error-level entry.
   *
   * @param {string} text Text to log
   * @param {boolean} showToast Also show this entry as a toast
   */
  error(text, showToast = false) {
    this.log(text, "danger");

    if (showToast) {
      $(document).Toasts("create", {
        class: "bg-danger",
        title: "Logger",
        autohide: true,
        delay: 15000,
        body: text,
        position: "bottomRight",
      });
    }
  }

  /**
   * Log a success-level entry.
   *
   * @param {string} text Text to log
   * @param {boolean} showToast Also show this entry as a toast
   */
  success(text, showToast = false) {
    this.log(text, "success");

    if (showToast) {
      $(document).Toasts("create", {
        class: "bg-success",
        title: "Logger (see Debug page to re-read)",
        autohide: true,
        delay: 15000,
        body: text,
        position: "bottomRight",
      });
    }
  }
}

/**
 * Special Modal class for constructing modals that only exist on the DOM when needed.
 * @requires $ jQuery
 * @requires createUUID()
 */
class Modal {
  /**
   * Construct the modal.
   *
   * @param {string} title Set the initial title
   * @param {?string} bgClass Set the initial color class for the modal background
   * @param {?string} body Set the initial body of the modal
   * @param {boolean} closeButton Should a close button be made in the top right corner?
   * @param {object} modalOptions Options to pass to iziModal or bootstrap modals
   */
  constructor(
    title = `Untitled Window`,
    bgClass,
    body = ``,
    closeButton = true,
    modalOptions = {}
  ) {
    if (!$) throw new Error("Modal requires jQuery");

    // Create a UUID for the modal.
    this.id = createUUID();

    // Set internal settings
    this._title = title;
    this.bgClass = bgClass;
    this._body = body;
    this.events = {};
    this.modalOptions = modalOptions;
    this.modalOptions.closeButton = closeButton;
    this.modalOptions.backdrop =
      this.modalOptions.overlayClose === false ? "static" : true;

    // Defaults
    this.modalOptions = Object.assign(
      $.fn.iziModal
        ? {
            headerColor: "#6c757d",
            focusInput: false,
            timeoutProgressbar: true,
            timeoutProgressbarColor: "#E32283",
            bodyOverflow: true,
            overlayClose: true,
          }
        : {
            show: true,
            focus: true,
          },
      this.modalOptions
    );
  }

  get title() {
    return this._title;
  }

  set title(value) {
    if (!$) throw new Error("Modal requires jQuery");

    this._title = value;
    if (this.izi) this.iziModal("setTitle", value);
  }

  // This returns the inner body DOM, not the actual content. The Modal needs to be open first before this can be used!
  get body() {
    return $(`#modal-${this.id}-body`);
  }

  set body(value) {
    if (!$) throw new Error("Modal requires jQuery");

    this._body = value;
    if (this.izi) this.iziModal("setContent", value);
  }

  /**
   * Add an event listener for the iziModal div container.
   *
   * @param {string} event The event
   * @param {function} fn The callback when the event is fired
   */
  addEvent(event, fn) {
    this.events[event] = fn;
    $(document).off(event, `#modal-${this.id}`);
    $(document).on(event, `#modal-${this.id}`, fn);
  }

  /**
   * Call iziModal. USE THIS opposed to calling this.izi.iziModal directly as this handles some internal things.
   *
   * @param {string} query The method to execute
   * @param {string|object} options A string or object of parameters to pass
   * @returns {?iziModal} the iziModal element (except when opening)
   */
  iziModal(query, options) {
    if (!$) throw new Error("Modal requires jQuery");

    if (query === "open" || query === "show") {
      // When opening the modal, we must first create it on the DOM

      if (!document.getElementById(`modal-${this.id}`)) {
        if ($.fn.iziModal) {
          $("body").append(
            `<div id="modal-${this.id}">
            <div id="modal-${this.id}-body" class="p-1 ${this.bgClass}">
            </div>
          </div>`
          );
        } else {
          $("body").append(
            `<div class="modal fade" id="modal-${
              this.id
            }" style="display: none;" aria-hidden="true">
				<div class="modal-dialog modal-lg">
				  <div class="modal-content ${this.bgClass}">
					<div class="modal-header">
					  <h4 class="modal-title" id="modal-${this.id}-title">${this._title}</h4>
					  ${
              this.modalOptions.closeButton
                ? `<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					  <span aria-hidden="true">×</span>
					</button>`
                : ``
            }
					</div>
					<div class="modal-body" id="modal-${this.id}-body">
					${this._body}
					</div>
				  </div>
				  <!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			  </div>`
          );
        }

        // Initialize the model once loaded in the DOM
        waitForElement(`#modal-${this.id}`, () => {
          if ($.fn.iziModal) {
            this.izi = $(`#modal-${this.id}`).iziModal(
              Object.assign({}, this.modalOptions, {
                title: this._title, // Always use this._title for the modal title
                afterRender: (modal) => {
                  // Show the modal after it is ready
                  modal.open(options);

                  // Just in case, reset the body content in the event this._body changed.
                  $(`#modal-${this.id}-body`).html(this._body);

                  // Perform any other afterRender stuff
                  if (typeof this.modalOptions.afterRender === "function")
                    this.modalOptions.afterRender(modal);
                },
                onClosed: (modal) => {
                  // Perform any other onClosed stuff first
                  if (typeof this.modalOptions.onClosed === "function")
                    this.modalOptions.onClosed(modal);

                  // After the modal is closed, we want to destroy the modal
                  try {
                    if (typeof modal.destroy === "function") modal.destroy();
                    this.izi = undefined;
                  } catch (e) {
                    // Ignore errors
                  }

                  // Now, we want to remove the DOM completely
                  $(`#modal-${this.id}`).remove();
                },
              })
            );

            // Re-set event listeners
            for (let event in this.events) {
              $(document).off(event, `#modal-${this.id}`);
              $(document).on(event, `#modal-${this.id}`, this.events[event]);
            }
          } else {
            this.izi = $(`#modal-${this.id}`).modal(
              Object.assign({}, this.modalOptions, { show: true, focus: true })
            );

            // Set important event listeners
            $(document).off(`show.bs.modal`, `#modal-${this.id}`);
            $(document).on(`show.bs.modal`, `#modal-${this.id}`, () => {
              // Reset title and body just in case
              $(`#modal-${this.id}-title`).html(this._title);
              $(`#modal-${this.id}-body`).html(this._body);

              // Execute event function
              if (this.events["show.bs.modal"]) this.events["show.bs.modal"]();
              if (typeof this.modalOptions.afterRender === "function")
                this.modalOptions.afterRender($(`#modal-${this.id}`));
            });
            $(document).off(`hidden.bs.modal`, `#modal-${this.id}`);
            $(document).on(`hidden.bs.modal`, `#modal-${this.id}`, () => {
              // Execute event function
              if (this.events["hidden.bs.modal"])
                this.events["hidden.bs.modal"]();
              if (typeof this.modalOptions.onClosed === "function")
                this.modalOptions.onClosed($(`#modal-${this.id}`));

              // After the modal is closed, we want to destroy the modal
              try {
                $(`#modal-${this.id}`).modal("dispose");
                this.izi = undefined;
              } catch (e) {
                // Ignore errors
              }

              // Now, we want to remove the DOM completely
              $(`#modal-${this.id}`).remove();

              return (
                $(".modal:visible").length &&
                $(document.body).addClass("modal-open")
              ); // Fix scrollbars
            });

            // Re-set other event listeners
            for (let event in this.events) {
              if (event === "show.bs.modal" || event === "hidden.bs.modal")
                continue; // We already defined these
              $(document).off(event, `#modal-${this.id}`);
              $(document).on(event, `#modal-${this.id}`, this.events[event]);
            }
          }
        });
      }

      return; // Do not continue
    }

    // Close is hide for bootstrap modals
    if (query === "close" && !$.fn.iziModal) query = "hide";

    if (query === "setTitle") {
      this._title = options; // If izi is used to set the title, make sure we update internally.
      if (!$.fn.iziModal) {
        $(`#modal-${this.id}-title`).html(this._title);
        $(`#modal-${this.id}`).modal("handleUpdate");
      }

      return this.izi;
    }
    if (query === "setContent") {
      // Set internal body content
      $(`#modal-${this.id}-body`).html(
        typeof options === "object" ? options.content : options
      );
      // If izi is used to set the body, make sure we update internally if default is true.
      if (typeof options === "object" && options.default)
        this._body = options.content;

      if (!$.fn.iziModal) {
        $(`#modal-${this.id}`).modal("handleUpdate");
      }

      return this.izi; // Do not continue
    }

    if (this.izi && this.izi.iziModal) {
      return this.izi.iziModal(query, options);
    } else if (this.izi && this.izi.modal) {
      return this.izi.modal(query);
    }
  }
}
