/**
 * Manager for Hue settings.
 * @requires Modal
 * @requires $ jQuery
 */
class HueSettings {
  /**
   * Construct the settings manager.
   * @param {HueManager} manager manager for the bulbs.
   */
  constructor(manager) {
    /**
     * Main HueManager.
     * @type HueManager
     * @readonly
     */
    this.manager = manager;

    /**
     * Cached settings for the app; should be set by renderer when main transmits via events.
     * @type object
     */
    this.settings = {};

    /**
     * Mapping of weather alert event (key) to their respective color (value).
     * @type object
     * @readonly
     */
    this.AlertColors = {
      "Tsunami Warning": "#FD6347",
      "Tornado Warning": "#FF0000",
      "Extreme Wind Warning": "#FF8C00",
      "Severe Thunderstorm Warning": "#FFA500",
      "Flash Flood Warning": "#8B0000",
      "Flash Flood Statement": "#8B0000",
      "Severe Weather Statement": "#00FFFF",
      "Shelter In Place Warning": "#FA8072",
      "Evacuation Immediate": "#7FFF00",
      "Civil Danger Warning": "#FFB6C1",
      "Nuclear Power Plant Warning": "#4B0082",
      "Radiological Hazard Warning": "#4B0082",
      "Hazardous Materials Warning": "#4B0082",
      "Fire Warning": "#A0522D",
      "Civil Emergency Message": "#FFB6C1",
      "Law Enforcement Warning": "#C0C0C0",
      "Storm Surge Warning": "#B524F7",
      "Hurricane Force Wind Warning": "#CD5C5C",
      "Hurricane Warning": "#DC143C",
      "Typhoon Warning": "#DC143C",
      "Special Marine Warning": "#FFA500",
      "Blizzard Warning": "#FF4500",
      "Snow Squall Warning": "#C71585",
      "Ice Storm Warning": "#8B008B",
      "Heavy Freezing Spray Warning": "#00BFFF",
      "Winter Storm Warning": "#FF69B4",
      "Lake Effect Snow Warning": "#008B8B",
      "Dust Storm Warning": "#FFE4C4",
      "Blowing Dust Warning": "#FFE4C4",
      "High Wind Warning": "#DAA520",
      "Tropical Storm Warning": "#B22222",
      "Storm Warning": "#9400D3",
      "Tsunami Advisory": "#D2691E",
      "Tsunami Watch": "#FF00FF",
      "Avalanche Warning": "#1E90FF",
      "Earthquake Warning": "#8B4513",
      "Volcano Warning": "#2F4F4F",
      "Ashfall Warning": "#A9A9A9",
      "Flood Warning": "#00FF00",
      "Coastal Flood Warning": "#228B22",
      "Lakeshore Flood Warning": "#228B22",
      "Ashfall Advisory": "#696969",
      "High Surf Warning": "#228B22",
      "Excessive Heat Warning": "#C71585",
      "Tornado Watch": "#FFFF00",
      "Severe Thunderstorm Watch": "#DB7093",
      "Flash Flood Watch": "#2E8B57",
      "Gale Warning": "#DDA0DD",
      "Flood Statement": "#00FF00",
      "Extreme Cold Warning": "#0000FF",
      "Freeze Warning": "#483D8B",
      "Red Flag Warning": "#FF1493",
      "Storm Surge Watch": "#DB7FF7",
      "Hurricane Watch": "#FF00FF",
      "Hurricane Force Wind Watch": "#9932CC",
      "Typhoon Watch": "#FF00FF",
      "Tropical Storm Watch": "#F08080",
      "Storm Watch": "#FFE4B5",
      "Tropical Cyclone Local Statement": "#FFE4B5",
      "Winter Weather Advisory": "#7B68EE",
      "Avalanche Advisory": "#CD853F",
      "Cold Weather Advisory": "#AFEEEE",
      "Heat Advisory": "#FF7F50",
      "Flood Advisory": "#00FF7F",
      "Coastal Flood Advisory": "#7CFC00",
      "Lakeshore Flood Advisory": "#7CFC00",
      "High Surf Advisory": "#BA55D3",
      "Dense Fog Advisory": "#708090",
      "Dense Smoke Advisory": "#F0E68C",
      "Small Craft Advisory": "#D8BFD8",
      "Brisk Wind Advisory": "#D8BFD8",
      "Hazardous Seas Warning": "#D8BFD8",
      "Dust Advisory": "#BDB76B",
      "Blowing Dust Advisory": "#BDB76B",
      "Lake Wind Advisory": "#D2B48C",
      "Wind Advisory": "#D2B48C",
      "Frost Advisory": "#6495ED",
      "Freezing Fog Advisory": "#008080",
      "Freezing Spray Advisory": "#00BFFF",
      "Low Water Advisory": "#A52A2A",
      "Local Area Emergency": "#C0C0C0",
      "Winter Storm Watch": "#4682B4",
      "Rip Current Statement": "#40E0D0",
      "Beach Hazards Statement": "#40E0D0",
      "Gale Watch": "#FFC0CB",
      "Avalanche Watch": "#F4A460",
      "Hazardous Seas Watch": "#483D8B",
      "Heavy Freezing Spray Watch": "#BC8F8F",
      "Flood Watch": "#2E8B57",
      "Coastal Flood Watch": "#66CDAA",
      "Lakeshore Flood Watch": "#66CDAA",
      "High Wind Watch": "#B8860B",
      "Excessive Heat Watch": "#800000",
      "Extreme Cold Watch": "#5F9EA0",
      "Freeze Watch": "#00FFFF",
      "Fire Weather Watch": "#FFDEAD",
      "Extreme Fire Danger": "#E9967A",
      "911 Telephone Outage": "#C0C0C0",
      "Coastal Flood Statement": "#6B8E23",
      "Lakeshore Flood Statement": "#6B8E23",
      "Special Weather Statement": "#FFE4B5",
      "Marine Weather Statement": "#FFDAB9",
      "Air Quality Alert": "#808080",
      "Air Stagnation Advisory": "#808080",
      "Hazardous Weather Outlook": "#EEE8AA",
      "Hydrologic Outlook": "#90EE90",
      "Short Term Forecast": "#98FB98",
      "Administrative Message": "#C0C0C0",
      Test: "#F0FFFF",
      "Child Abduction Emergency": "#FFFFFF",
      "Blue Alert": "#FFFFFF",
    };

    /**
     * Main modal for managing settings.
     * @type Modal
     */
    this.modal = new Modal(`Settings`, ``, ``, true, {
      overlayClose: false,
    });
  }

  /**
   * Initialize the settings form for location settings.
   */
  showLocationSettings(dom) {
    this.modal.iziModal("open");
    this.modal.title = "Location Settings";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "object",
        properties: {
          lat: {
            title: "Latitude",
            type: "number",
            minimum: -180,
            maximum: 180,
            required: true,
          },
          lon: {
            title: "Longitude",
            type: "number",
            minimum: -180,
            maximum: 180,
            required: true,
          },
        },
      },
      options: {
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [`location`, value]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Location settings saved",
                  autohide: true,
                  delay: 5000,
                  body: `Location settings have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.location,
    });
  }

  /**
   * Initialize the settings form for Hue Bridge settings.
   */
  showHueSettings(dom) {
    this.modal.iziModal("open");
    this.modal.title = "Hue Settings";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "object",
        properties: {
          ipAddress: {
            title: "IP Address",
            type: "string",
          },
          username: {
            title: "API Username",
            type: "string",
            format: "password",
          },
          clientKey: {
            title: "API Client Key",
            type: "string",
            format: "password",
          },
        },
      },
      options: {
        fields: {
          ipAddress: {
            helper:
              "Leave blank or set to blank to try and re-discover your Hue Bridge.",
          },
          username: {
            helper:
              "The username for accessing / authenticating the Hue Bridge API locally. You should use the create API user tool to create a new user when necessary.",
          },
          clientKey: {
            helper:
              "The client key for accessing / authenticating the Hue Bridge API locally. You should use the create API user tool to create a new user when necessary.",
          },
        },
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [`hue`, value]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Hue settings saved",
                  autohide: true,
                  delay: 5000,
                  body: `Hue settings have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.hue,
    });
  }

  /**
   * Adjust sun brightness settings
   */
  showSunSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Sun Settings";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "object",
        properties: {
          minBrightness: {
            title: "Minimum Brightness",
            type: "number",
            minimum: 0,
            maximum: 100,
            required: true,
          },
          maxBrightness: {
            title: "Maximum Brightness",
            type: "number",
            minimum: 0,
            maximum: 100,
            required: true,
          },
        },
      },
      options: {
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [`sun.sun`, value]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Sun settings saved",
                  autohide: true,
                  delay: 5000,
                  body: `Sun settings have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.sun.sun,
    });
  }

  /**
   * Adjust settings for kelvin
   */
  showKelvinSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Kelvin Settings";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "object",
        properties: {
          minAtHour: {
            title: "Reach Mininum Kelvin At This Hour",
            type: "number",
            minimum: 0,
            maximum: 24,
            required: true,
          },
          maxAtHour: {
            title: "Switch to Maximum Kelvin At This Hour",
            type: "number",
            minimum: 0,
            maximum: 24,
            required: true,
          },
          min: {
            title: "Minimum Kelvin Degrees",
            type: "number",
            minimum: 1500,
            maximum: 9000,
            required: true,
          },
          max: {
            title: "Maximum Kelvin Degrees",
            type: "number",
            minimum: 1500,
            maximum: 9000,
            required: true,
          },
        },
      },
      options: {
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [`sun.kelvin`, value]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Kelvin settings saved",
                  autohide: true,
                  delay: 5000,
                  body: `Kelvin settings have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.sun.kelvin,
    });
  }

  /**
   * Specify which bulbs adjust according to the sun.
   */
  showSunBulbSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Sun Bulbs";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "array",
        title: "Bulbs That Adjust Brightness According to Sun",
        items: {
          type: "string",
          enum: Object.keys(this.manager.bulbs),
        },
      },
      options: {
        type: "checkbox",
        optionLabels: Object.values(this.manager.bulbs).map(
          (bulb) => `${bulb.name} (${bulb.uniqueid})`
        ),
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [`sun.bulbsSun`, value]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Sun bulbs saved",
                  autohide: true,
                  delay: 5000,
                  body: `Sun bulbs have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.sun.bulbsSun,
    });
  }

  /**
   * Specify which bulbs adjust kelvin according to time.
   */
  showKelvinBulbSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Kelvin Bulbs";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "array",
        title: "Bulbs That Adjust Kelvin according to Time",
        items: {
          type: "string",
          enum: Object.keys(this.manager.bulbs),
        },
      },
      options: {
        type: "checkbox",
        optionLabels: Object.values(this.manager.bulbs).map(
          (bulb) => `${bulb.name} (${bulb.uniqueid})`
        ),
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [`sun.bulbsKelvin`, value]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Kelvin bulbs saved",
                  autohide: true,
                  delay: 5000,
                  body: `Kelvin bulbs have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.sun.bulbsKelvin,
    });
  }

  /**
   * Change OpenWeatherMap settings
   */
  showTempestSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Tempest Settings";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "object",
        properties: {
          stationId: {
            type: "number",
            required: true,
            title: "Station ID",
          },
          token: {
            type: "string",
            format: "password",
            required: true,
            title: "Tempest Access Token",
          },
        },
      },
      options: {
        fields: {
          stationId: {
            helper: "https://swd.weatherflow.com/swd/rest/stations?token=",
          },
          token: {
            helper: "https://weatherflow.github.io/Tempest/api/",
          },
        },
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [`weather.tempest`, value]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Tempest settings saved",
                  autohide: true,
                  delay: 5000,
                  body: `Tempest settings have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.weather.tempest,
    });
  }

  /**
   * Adjust Sun Brightness settings.
   */
  showSunBrightnessSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Sun Brightness Settings";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "object",
        properties: {
          minBrightness: {
            title: "Minimum Bulb Brightness",
            type: "number",
            minimum: 0,
            maximum: 100,
            required: true,
          },
          maxBrightness: {
            title: "Maximum Bulb Brightness",
            type: "number",
            minimum: 0,
            maximum: 100,
            required: true,
          },
          minLux: {
            title: "Minimum station brightness (lux)",
            type: "number",
            minimum: 0,
            required: true,
          },
          maxLux: {
            title: "Maximum station brightness (lux)",
            type: "number",
            minimum: 0,
            required: true,
          },
        },
      },
      options: {
        fields: {
          minLux: {
            helper:
              "When Tempest station brightness is at or below this many lux, bulbs will reach defined maximum brightness.",
          },
          maxLux: {
            helper:
              "When Tempest station brightness is at or above this many lux, bulbs will reach defined minimum brightness.",
          },
        },
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [
                  `weather.sunBrightness`,
                  value,
                ]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Sun Brightness settings saved",
                  autohide: true,
                  delay: 5000,
                  body: `Sun Brightness settings have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.weather.sunBrightness,
    });
  }

  /**
   * Choose bulbs that adjust brightness according to Sun Brightness.
   */
  showSunBrightnessBulbSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Sun Brightness Bulbs";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "array",
        title: "Bulbs That Adjust Brightness According to Sun Brightness",
        items: {
          type: "string",
          enum: Object.keys(this.manager.bulbs),
        },
      },
      options: {
        type: "checkbox",
        optionLabels: Object.values(this.manager.bulbs).map(
          (bulb) => `${bulb.name} (${bulb.uniqueid})`
        ),
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [
                  `weather.bulbsSunBrightness`,
                  value,
                ]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Sun Brightness bulbs saved",
                  autohide: true,
                  delay: 5000,
                  body: `Sun Brightness bulbs have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.weather.bulbsSunBrightness,
    });
  }

  /**
   * Set which bulbs should gently flash for current or incoming precipitation.
   */
  showPrecipitationBulbSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Precipitation Bulbs";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "array",
        title: "Bulbs that react for precipitation",
        items: {
          type: "string",
          enum: Object.keys(this.manager.bulbs),
        },
      },
      options: {
        type: "checkbox",
        optionLabels: Object.values(this.manager.bulbs).map(
          (bulb) => `${bulb.name} (${bulb.uniqueid})`
        ),
        helpers: [
          "These bulbs will turn a slight cyan if precipitation is in the near-term forecast.",
          "These bulbs will turn a moderate cyan if precipitation has been detected now.",
          "These bulbs will return to their previous known color when precipitation is no-longer in the forecast.",
          "Precipitation bulbs which are also lightning bulbs will prioritize lightning detected over everything, and lightning in the forecast over precipitation in the forecast.",
        ],
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [
                  `weather.bulbsPrecipitation`,
                  value,
                ]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Precipitation bulbs saved",
                  autohide: true,
                  delay: 5000,
                  body: `Precipitation bulbs have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.weather.bulbsPrecipitation,
    });
  }

  /**
   * Choose bulbs that adjust brightness according to lightning.
   */
  showLightningBulbSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Lightning Bulbs";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "array",
        title: "Bulbs that react for lightning",
        items: {
          type: "string",
          enum: Object.keys(this.manager.bulbs),
        },
      },
      options: {
        type: "checkbox",
        optionLabels: Object.values(this.manager.bulbs).map(
          (bulb) => `${bulb.name} (${bulb.uniqueid})`
        ),
        helpers: [
          "These bulbs will turn a slight yellow if lightning is in the near-term forecast.",
          "These bulbs will turn a moderate yellow if lightning was detected in the last 30 minutes within a 30 km area.",
          "These bulbs will return to their previous known color when lightning is no-longer in the forecast.",
          "If these bulbs are off, they will turn on for lightning.",
          "Lightning bulbs which are also precipitation bulbs will prioritize lightning detected over everything, and lightning in the forecast over precipitation in the forecast.",
        ],
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [
                  `weather.bulbsLightning`,
                  value,
                ]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Lightning bulbs saved",
                  autohide: true,
                  delay: 5000,
                  body: `Lightning bulbs have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.weather.bulbsLightning,
    });
  }

  /**
   * Configure which alerts should flash the bulbs, and see how flashing works.
   */
  showWeatherAlertsSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Weather Alert Settings";
    this.modal.body = `<div class="callout callout-warning">
<h4>Flash Colors</h4>
<p>When bulbs flash to indicate weather alerts, the color will match the weather alert as shown in the table below.</p>
</div>

<div class="callout callout-danger">
<h4>New Alerts</h4>
<p>When a new alert is detected, configured bulbs will flash for one minute with the alert color.</p>
<p>Extreme and severe alerts will flash even if the bulb is off. Moderate and Minor alerts will not flash bulbs set to off.</p>
</div>

<div class="callout callout-info">
<h4>Repeat Flashing / 5-minute Sequence</h4>
<p>Extreme alerts will repeatedly flash configured bulbs red for 15 seconds every minute until the alert is no longer in effect.</p>
<p>Configured 5-minute sequence bulbs will flash-cycle through the colors of the alerts currently in effect every 5 minutes (but only alerts whose severity is applicable to the bulb). Minor and moderate alerts will flash once, and severe and extreme alerts will flash for 15 seconds.</p>
</div>
<table id="section-settings-alerts-alerts-table" class="table table-striped" style="min-width: 100%;"></table>`;

    window.requestAnimationFrame(() => {
      // Build table rows
      let dataSource = [];
      for (let alert in this.AlertColors) {
        if (!Object.prototype.hasOwnProperty.call(this.AlertColors, alert))
          continue;
        let column1 = alert;
        let column2 = `<i class="fas fa-circle" style="color: ${this.AlertColors[alert]};"></i>`;
        let column3 = `<input type="checkbox" class="section-settings-alerts-alerts-table-checkbox" data-alert="${alert}"${
          this.settings.NWS.alerts[alert] ? ` checked` : ``
        }>`;
        dataSource.push([column1, column2, column3]);
      }

      // Create the data table
      $("#section-settings-alerts-alerts-table").DataTable({
        scrollCollapse: true,
        paging: false,
        data: dataSource,
        columns: [
          { title: "Alert Name" },
          { title: "Color" },
          { title: "Flash for this?" },
        ],
        columnDefs: [{ responsivePriority: 1, targets: 2 }],
        drawCallback: () => {
          // Checkbox click events
          $(".section-settings-alerts-alerts-table-checkbox").off("click");

          $(".section-settings-alerts-alerts-table-checkbox").on(
            "click",
            (e) => {
              let clickedAlert = $(e.currentTarget).data("alert");
              let checked = $(`input[data-alert="${clickedAlert}"]`).prop(
                "checked"
              );
              window.ipc.main.send("settings", [
                `NWS.alerts.${clickedAlert}`,
                checked,
              ]);
            }
          );
        },
      });
    });
  }

  /**
   * Configure Hue bulbs for weather alerts.
   */
  showWeatherAlertsBulbSettings() {
    this.modal.iziModal("open");
    this.modal.title = "Weather Alerts Bulbs";
    this.modal.body = "";
    $(this.modal.body).alpaca({
      schema: {
        type: "object",
        properties: {
          Extreme: {
            type: "array",
            title: "Bulbs that Flash for Extreme Alerts",
            items: {
              type: "string",
              enum: Object.keys(this.manager.bulbs),
            },
          },
          Severe: {
            type: "array",
            title: "Bulbs that Flash for Severe Alerts",
            items: {
              type: "string",
              enum: Object.keys(this.manager.bulbs),
            },
          },
          Moderate: {
            type: "array",
            title: "Bulbs that Flash for Moderate Alerts",
            items: {
              type: "string",
              enum: Object.keys(this.manager.bulbs),
            },
          },
          Minor: {
            type: "array",
            title: "Bulbs that Flash for Minor Alerts",
            items: {
              type: "string",
              enum: Object.keys(this.manager.bulbs),
            },
          },
          sequence: {
            type: "array",
            title: "Bulbs that Cycle Active Alerts Every 5 Minutes",
            items: {
              type: "string",
              enum: Object.keys(this.manager.bulbs),
            },
          },
        },
      },
      options: {
        fields: {
          Extreme: {
            type: "checkbox",
            optionLabels: Object.values(this.manager.bulbs).map(
              (bulb) => `${bulb.name} (${bulb.uniqueid})`
            ),
            helpers: [
              "Extreme alerts pose a significant threat to life and/or property.",
              "These bulbs will flash for extreme alerts even when set to off.",
              "These bulbs will flash for 15 seconds every minute until the alert expires.",
            ],
          },
          Severe: {
            type: "checkbox",
            optionLabels: Object.values(this.manager.bulbs).map(
              (bulb) => `${bulb.name} (${bulb.uniqueid})`
            ),
            helpers: [
              "Severe alerts pose a high threat to life and/or property.",
              "These bulbs will flash for severe alerts even when set to off.",
            ],
          },
          Moderate: {
            type: "checkbox",
            optionLabels: Object.values(this.manager.bulbs).map(
              (bulb) => `${bulb.name} (${bulb.uniqueid})`
            ),
            helper: "Moderate alerts pose a mild risk to life and/or property.",
          },
          Minor: {
            type: "checkbox",
            optionLabels: Object.values(this.manager.bulbs).map(
              (bulb) => `${bulb.name} (${bulb.uniqueid})`
            ),
            helper:
              "Minor alerts pose little to no threat to life or property.",
          },
          sequence: {
            type: "checkbox",
            optionLabels: Object.values(this.manager.bulbs).map(
              (bulb) => `${bulb.name} (${bulb.id})`
            ),
            helpers: [
              "These bulbs will flash-cycle through active weather alerts every 5 minutes.",
              "These bulbs must also be set to flash for your desired severities; they will only flash-cycle alerts whose severity is applicable to the bulb according to the other settings.",
            ],
          },
        },
        form: {
          buttons: {
            submit: {
              title: "Save Settings",
              click: (form, e) => {
                form.refreshValidationState(true);
                if (!form.isValid(true)) {
                  form.focus();
                  return;
                }
                var value = form.getValue();
                window.ipc.main.send("settings", [`NWS.bulbs`, value]);

                this.modal.iziModal("close");

                $(document).Toasts("create", {
                  class: "bg-success",
                  title: "Weather alerts bulbs saved",
                  autohide: true,
                  delay: 5000,
                  body: `Weather alerts bulbs have been saved`,
                  position: "bottomRight",
                });
              },
            },
          },
        },
      },
      data: this.settings.NWS.bulbs,
    });
  }

  /**
   * Specify new / updated settings.
   *
   * @param {object} settings Updated app settings
   */
  newSettings(settings) {
    this.settings = settings;
  }
}
